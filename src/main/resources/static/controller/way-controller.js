let wayManagement = angular.module("way-management", ['ngRoute']);

wayManagement.run(function ($window) {
    if ($window.localStorage.jwtToken) {
        $window.location.replace('#!');
    } else {
        $window.location.replace('#!login');
    }
})

wayManagement.controller("way-controller", function ($scope, $http, $window) {
    $scope.disableCheckBox = true

    //Set Jwt token to every request header in this controller
    $http.defaults.headers.common.Authorization = $window.localStorage.jwtToken;
    $scope.showMap = function () {
        if (!mapboxgl.accessToken) {
            showMapBoxMapWithToken($http)
        }
        $scope.show = !$scope.show;
    };

    $scope.getWaysFromBbox = function getTestWays() {
        console.log('First bbox point: ' + firstPoint);
        console.log('Second bbox point: ' + secondPoint);
        const linesId = 'lines';
        const linesNoParking = 'linesNoParking';
        const linesRestriction = 'linesRestriction';
        removeSourceAndLayer([linesId, linesNoParking, linesRestriction]);
        getWays($http, linesId)
        $scope.disableCheckBox = false;
        $scope.checkBox = false;
    }

    $scope.logout = function () {
        $window.localStorage.removeItem('jwtToken');
        $http.defaults.headers.common.Authorization = null;
        $window.location.replace('#!login')
    }

    $scope.showNoParking = function () {
        const linesId = 'linesNoParking';
        if (map.getLayer(linesId)) {
            showHideLayer(linesId)
        } else {
            console.log('First bbox point: ' + firstPoint);
            console.log('Second bbox point: ' + secondPoint);
            getNoParkingWays($http, linesId)
        }
    }

    $scope.removeBbox = function () {
        removeBoundingBox();
    }

    $scope.getRestrictionSegment = function () {
        const linesId = 'linesRestriction';
        if (map.getLayer(linesId)) {
            showHideLayer(linesId)
        } else {
            getRestrictionSegments($http, linesId);
        }
    }
});
