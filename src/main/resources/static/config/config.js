wayManagement.factory('loginInterceptor', ['$q', '$window', function ($q, $window) {
    return {
        responseError: function (response) {
            console.log('Failed with', response.status, 'status');
            if (response.status === 403 || response.status === 401) {
                $window.location.replace('#!login');
            }
            return $q.reject(response);
        }
    }
}])

wayManagement.config(function ($httpProvider) {
    $httpProvider.interceptors.push('loginInterceptor')
})

wayManagement.config(function ($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: 'views/mapbox.html',
        controller: 'way-controller',
    })
    $routeProvider.when("/login", {
        templateUrl: 'views/login.html',
        controller: 'way-controller',
    })
})
