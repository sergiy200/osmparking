class Layer {
    constructor(id, type, source, paint) {
        this.id = id;
        this.type = type;
        this.source = source;
        this.paint = paint;
    }

    getId() {
        return this.id
    }

    getType() {
        return this.type
    }

    getSource() {
        return this.source
    }

    getPaint() {
        return this.paint
    }

}