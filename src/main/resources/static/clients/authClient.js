function authorization($scope, $http, $window) {
    $http.post('/login', {
        "login": $scope.user.login,
        "password": $scope.user.password
    }).then(function successCallback(headers) {
        $window.localStorage.jwtToken = headers.headers()['authorization'];
        $window.location.href = '/'
    }, function errorCallback(response) {
        console.log(response.statusText);
    });
}