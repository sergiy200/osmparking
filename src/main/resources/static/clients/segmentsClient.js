function getWays($http, linesId) {
    $http.get('/segments/way', {
        params: {
            leftLatitude: firstPoint.lat,
            leftLongitude: firstPoint.lng,
            rightLatitude: secondPoint.lat,
            rightLongitude: secondPoint.lng
        }
    }).then(
        function successCallback(response) {
            let paint = {
                'line-width': 3,
                'line-color': '#25831c',
            };
            let lineLayer = new Layer(linesId, 'line', linesId, paint);
            drawLines(lineLayer, response.data);
        },
        function errorCallback(response) {
            console.log(response);
        });
}

function getRestrictionSegments($http, linesId) {
    $http.get('/segments/restrict', {
        params: {
            leftLatitude: firstPoint.lat,
            leftLongitude: firstPoint.lng,
            rightLatitude: secondPoint.lat,
            rightLongitude: secondPoint.lng
        }
    }).then(
        function successCallback(response) {
            let paint = {
                'line-width': 3,
                'line-color': '#ffc400',
            };
            let lineLayer = new Layer(linesId, 'line', linesId, paint);
            removeSourceAndLayer([linesId]);
            drawLines(lineLayer, setDescriptionToFeatureCollections(response));
            setDescription(lineLayer.id)
        },
        function errorCallback(response) {
            console.log(response);
        });
}

function getNoParkingWays($http, linesId) {
    $http.get('/segments/noparking-mapbox', {
        params: {
            leftLatitude: firstPoint.lat,
            leftLongitude: firstPoint.lng,
            rightLatitude: secondPoint.lat,
            rightLongitude: secondPoint.lng
        }
    }).then(
        function successCallback(response) {
            let paint = {
                'line-width': 3,
                'line-color': '#ff3700',
            };
            let lineLayer = new Layer(linesId, 'line', linesId, paint);
            drawLines(lineLayer, response.data);
        },
        function errorCallback(response) {
            console.log(response);
        });
}