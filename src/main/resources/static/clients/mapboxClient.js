function showMapBoxMapWithToken($http) {
    $http.get("/segments/access-token").then(function successCallback(headers) {
            createMap(headers.headers()['mapbox-token']);
        },
        function errorCallback(response) {
            console.log(response);
        });
}
