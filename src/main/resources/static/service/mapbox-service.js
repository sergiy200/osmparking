let map;

function createMap(token) {
    mapboxgl.accessToken = token
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [13.43078804, 52.468372345],
        zoom: 12
    });

    map.on('click', (e) => {
        createBbox(e);
    })
}

function createSource(sourceId, data) {
    map.addSource(sourceId, {
        'type': 'geojson',
        'data': data
    });
}

function createLayer(layer) {
    map.addLayer({
        'id': layer.id,
        'type': layer.type,
        'source': layer.id,
        'paint': layer.paint,
        'layout': {
            'visibility': 'visible'
        }
    });
}

function removeSourceAndLayer(arrayIds) {
    arrayIds.forEach(id => {
        if (map.getLayer(id)) {
            map.removeLayer(id);
        }
        if (map.getSource(id)) {
            map.removeSource(id);
        }
    });

}

function showHideLayer(id) {
    let visibility = map.getLayoutProperty(id, 'visibility');
    if (visibility === 'visible') {
        map.setLayoutProperty(id, 'visibility', 'none');
    } else {
        map.setLayoutProperty(id, 'visibility', 'visible');
    }
}

function setDescription(layerId) {
    map.on('click',layerId, (e) => {
        const description = e.features[0].properties.description;

        new mapboxgl.Popup()
            .setLngLat(e.lngLat)
            .setText(description)
            .addTo(map);
    })
}
