let clickCount = 1;
let firstPoint;
let secondPoint;
const bboxId = "bbox";

function createBbox(e) {

    if (clickCount === 1) {
        firstPoint = e.lngLat
    }
    if (clickCount === 2) {
        secondPoint = e.lngLat;
        drawBoundingBox(bboxId, firstPoint, secondPoint);
    }
    clickCount++;
}

function removeBoundingBox() {
    removeSourceAndLayer([bboxId])
    clickCount = 1;
}