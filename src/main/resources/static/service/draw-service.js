function drawLines(lineLayer, responseData) {
    createSource(lineLayer.id, responseData);
    createLayer(lineLayer)
}

function drawBoundingBox(bboxId, start, end) {
    let paint = {
        'fill-color': '#2594ff',
        'fill-opacity': 0.5
    };
    let polygonLayer = new Layer(bboxId, 'fill', bboxId, paint);
    let polygon = {
        'type': 'Feature',
        'geometry': {
            'type': 'Polygon',
            'coordinates': [
                [
                    [start.lng, start.lat],
                    [start.lng, end.lat],
                    [end.lng, end.lat],
                    [end.lng, start.lat],
                    [start.lng, start.lat]
                ]
            ]
        }
    }
    createSource(bboxId, polygon)
    createLayer(polygonLayer)
}

function setDescriptionToFeatureCollections(response) {
    let featureCollection = {
        'type': 'FeatureCollection',
        'features': []
    }
    response.data.forEach(res => {
        res.geoJsonFeatureCollection['properties'] = {
            'description': res.wayRestrictionTimes
        }
        featureCollection.features.push(res.geoJsonFeatureCollection);
    });
    return featureCollection
}
