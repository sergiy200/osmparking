package com.example.osmparking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsmParkingApplication {

    public static void main(String[] args) {
        SpringApplication.run(OsmParkingApplication.class, args);
    }
}
