package com.example.osmparking.service;

public interface GeometryService {

    boolean matchIntersectionByPercent(String geometry, String intersectGeometry, double percent);

    String buffered(String geometry);
}
