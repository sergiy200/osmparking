package com.example.osmparking.service;

import com.example.osmparking.model.BoundingBox;

import java.util.List;

public interface OSMApiService {
    <T> List<T> getElements(BoundingBox boundingBox, String queryPattern, Class<T> tClass);
}
