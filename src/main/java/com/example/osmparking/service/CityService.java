package com.example.osmparking.service;

import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.CityGeometryDto;

import java.util.List;

public interface CityService {
    List<City> getAll();

    boolean saveCitiesFromCSV(String fileName);

    List<City> getCities(String prefix);

    boolean addCity(CityDto cityDto);

    boolean delete(String prefix);

    List<CityGeometryDto> getCityGeometry();
}
