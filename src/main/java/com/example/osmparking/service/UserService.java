package com.example.osmparking.service;

import com.example.osmparking.model.dto.LoginDto;
import com.example.osmparking.model.dto.RegisterDto;

import java.util.Optional;

public interface UserService {
    boolean register(RegisterDto registerDto);

    Optional<String> login(LoginDto loginDto);
}
