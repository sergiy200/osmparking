package com.example.osmparking.service;

public interface DayOfWeekParserService {

   Integer getDayOfWeek(String days);
}
