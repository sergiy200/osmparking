package com.example.osmparking.service;

import java.io.Reader;
import java.util.List;

public interface ReadCSVService {
    <T> List<T> csvReader(Reader reader, Class<T> clazz, int lineToSkip);
}
