package com.example.osmparking.service;

import java.util.List;

public interface S3ReaderService {
   <T> List<T> getDataFromBucket(String bucketName, String fileName, Class<T> tClass, int lineToSkip);
}
