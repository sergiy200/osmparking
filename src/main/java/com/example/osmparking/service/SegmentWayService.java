package com.example.osmparking.service;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.SegmentGeoJsonRestrictionTime;

import java.util.List;

public interface SegmentWayService {
    List<SegmentGeoJsonRestrictionTime> getGeoJsonRestrictionSegment(BoundingBox boundingBox);
}
