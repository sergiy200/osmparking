package com.example.osmparking.service;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.dto.SegmentRestrictionTime;

import java.util.List;

public interface WaysService {
    List<SegmentRestrictionTime> getSegmentWithRestrictionTimes(BoundingBox boundingBox);
}
