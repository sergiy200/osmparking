package com.example.osmparking.service;

public interface CitySegmentService {
    boolean save(String fileName);

    void updateCityPrefixToSegmentsByJTS();

    boolean updateCityPrefixByPostGis();
}
