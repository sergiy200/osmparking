package com.example.osmparking.service;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;

public interface TimeService {
    Date getCurrentTimeMillis();

    Integer[] getDaysOfWeek(String daysRange);

    List<String[]> getTime(String time);

    LocalTime getLocalTime(String localTime);
}
