package com.example.osmparking.service.impl;

import com.example.osmparking.service.MapboxService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MapboxServiceImpl implements MapboxService {

    @Value("${MAPBOX_TOKEN}")
    private String mapboxToken;

    @Override
    public String getToken() {
        return mapboxToken;
    }
}
