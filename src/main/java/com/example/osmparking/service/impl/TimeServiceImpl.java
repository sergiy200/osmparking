package com.example.osmparking.service.impl;

import com.example.osmparking.service.DayOfWeekParserService;
import com.example.osmparking.service.TimeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.apache.commons.lang3.StringUtils.leftPad;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

@Slf4j
@Service
@RequiredArgsConstructor
public class TimeServiceImpl implements TimeService {

    private static final String TIME_24 = "24:00";
    private static final String LOCAL_TIME_24 = "23:59:59";

    private final DayOfWeekParserService dayOfWeekParserService;

    @Override
    public Date getCurrentTimeMillis() {
        return new Date(System.currentTimeMillis());
    }

    @Override
    public Integer[] getDaysOfWeek(String daysRange) {
        if (daysRange.contains(",")) {
            return getDayOfWeekByComma(daysRange);
        }
        String[] days = daysRange.split("-");
        int startRange = dayOfWeekParserService.getDayOfWeek(days[INTEGER_ZERO]);
        int endRange = days.length == 1 ? startRange : dayOfWeekParserService.getDayOfWeek(days[INTEGER_ONE]);
        if (startRange > endRange) {
            return getDaysFromTwoWeeks(startRange, endRange);
        }
        return getRangeList(startRange, endRange)
                .toArray(Integer[]::new);
    }

    private Integer[] getDayOfWeekByComma(String daysRange) {
        return Stream.of(daysRange.split(","))
                .map(dayOfWeekParserService::getDayOfWeek)
                .toArray(Integer[]::new);
    }

    private Integer[] getDaysFromTwoWeeks(int startRange, int endRange) {
        List<Integer> daysList = new ArrayList<>();
        daysList.addAll(getRangeList(startRange, 7));
        daysList.addAll(getRangeList(1, endRange));
        return daysList.toArray(Integer[]::new);
    }

    @Override
    public List<String[]> getTime(String time) {
        return Arrays
                .stream(time.split("[,]"))
                .map(timeInterval -> timeInterval.split("-"))
                .collect(toUnmodifiableList());
    }

    @Override
    public LocalTime getLocalTime(String localTime) {
        try {
            if (localTime.equals(TIME_24)) {
                localTime = LOCAL_TIME_24;
            }
            return LocalTime.parse(leftPad(localTime, 5, '0'));
        } catch (Exception e) {
            log.error("Illegal argument to parse {}", localTime);
            throw new RuntimeException("Illegal argument to parse {}", e);
        }
    }

    private List<Integer> getRangeList(int startInclusive, Integer second) {
        return IntStream
                .rangeClosed(startInclusive, second)
                .boxed()
                .collect(toUnmodifiableList());
    }
}
