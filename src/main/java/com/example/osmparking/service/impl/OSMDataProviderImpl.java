package com.example.osmparking.service.impl;

import com.example.osmparking.model.*;
import com.example.osmparking.model.dto.NoParkingSegment;
import com.example.osmparking.model.dto.NoParkingTags;
import com.example.osmparking.model.dto.ParkingTimeTags;
import com.example.osmparking.model.enums.Side;
import com.example.osmparking.service.OSMApiService;
import com.example.osmparking.service.OSMDataProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@Service
@RequiredArgsConstructor
public class OSMDataProviderImpl implements OSMDataProvider {

    private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
    private final OSMApiService osmApiService;
    private final ObjectMapper objectMapper;
    private final TimeParserServiceImpl timeParserService;

    @Override
    public List<NoParkingSegment> getNoParkingSegments(BoundingBox boundingBox, String query) {
        return osmApiService.getElements(boundingBox, query, OSMWay.class)
                .stream()
                .map(way -> new NoParkingSegment(way.getId(),
                        getLineStringFromPoints(way.getGeometry()).toString(),
                        getTags(way.getTags(), NoParkingTags.class)))
                .collect(toUnmodifiableList());
    }

    @Override
    public List<RestrictionTime> getRestrictionTimes(BoundingBox boundingBox, String query) {
        return osmApiService.getElements(boundingBox, query, OSMWay.class)
                .stream()
                .flatMap(way -> getRestrictionByParkingTimeTags(way.getId(), getTags(way.getTags(), ParkingTimeTags.class)))
                .collect(toUnmodifiableList());
    }

    @Override
    public List<Ways> getWays(BoundingBox boundingBox, String query) {
        return osmApiService.getElements(boundingBox, query, OSMWay.class)
                .stream()
                .map(way -> new Ways(way.getId(),
                        getLineStringFromPoints(way.getGeometry()).toString(),
                        getHighwayFromTags(way.getTags()).orElse(EMPTY)))
                .collect(toUnmodifiableList());
    }

    private Stream<RestrictionTime> getRestrictionByParkingTimeTags(long wayId, ParkingTimeTags parkingTimeTags) {
        return Stream.of(Side.values())
                .flatMap(side -> getTimeSideList(wayId, parkingTimeTags, side));
    }

    private Stream<RestrictionTime> getTimeSideList(long wayId, ParkingTimeTags parkingTimeTags, Side side) {
        return side.getParkingTime().apply(parkingTimeTags)
                .map(time -> timeParserService.createRestrictionTime(wayId,
                        timeParserService.getTimeInterval(time),
                        side.getName()))
                .orElse(Collections.emptyList())
                .stream();
    }

    private <T> T getTags(LinkedHashMap<String, String> tags, Class<T> clazz) {
        return objectMapper.convertValue(tags, clazz);
    }

    private Optional<String> getHighwayFromTags(LinkedHashMap<String, String> tags) {
        return Optional.ofNullable(tags.get("highway"));
    }

    private Geometry getLineStringFromPoints(List<Point> points) {
        return GEOMETRY_FACTORY
                .createLineString(getCoordinates(points));
    }

    private Coordinate[] getCoordinates(List<Point> points) {
        return points.stream()
                .map(point -> new Coordinate(point.getLon(), point.getLat()))
                .toArray(Coordinate[]::new);
    }
}
