package com.example.osmparking.service.impl;

import com.example.osmparking.model.Segment;
import com.example.osmparking.service.CityService;
import com.example.osmparking.service.CitySegmentService;
import com.example.osmparking.service.SegmentsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CitySegmentServiceImpl implements CitySegmentService {

    private final SegmentsService segmentsService;
    private final CityService cityService;

    @Override
    public boolean save(String fileName) {
        List<Segment> segments = segmentsService.getSegmentsFromCSV(fileName);
        return segmentsService.save(segments);
    }

    @Override
    public void updateCityPrefixToSegmentsByJTS() {
        segmentsService.setCityPrefixToSegments(segmentsService.getSegmentGeometry(), cityService.getCityGeometry());
    }

    @Override
    public boolean updateCityPrefixByPostGis() {
        return segmentsService.updateSegmentsCities(segmentsService.getCityPrefixWithIntersectedSegment());
    }
}
