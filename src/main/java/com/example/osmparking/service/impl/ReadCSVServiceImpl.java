package com.example.osmparking.service.impl;

import com.example.osmparking.service.ReadCSVService;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReadCSVServiceImpl implements ReadCSVService {

    @Override
    public <T> List<T> csvReader(Reader reader, Class<T> clazz, int lineToSkip) {
        return new CsvToBeanBuilder<T>(reader)
                .withType(clazz)
                .withSkipLines(lineToSkip)
                .build()
                .parse();
    }
}
