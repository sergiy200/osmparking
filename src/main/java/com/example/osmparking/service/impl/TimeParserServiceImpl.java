package com.example.osmparking.service.impl;

import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.TimeInterval;
import com.example.osmparking.service.TimeParserService;
import com.example.osmparking.service.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

@Service
@RequiredArgsConstructor
public class TimeParserServiceImpl implements TimeParserService {

    private static final String REG_EXPRESSION_TIME = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
    private static final String ALL_WEEK = "Mo-Su";
    private final TimeService timeService;

    @Override
    public List<TimeInterval> getTimeInterval(String timeInterval) {
        return Arrays.stream(getSplitByRegex(timeInterval, ";"))
                .map(time -> getInterval(getSplitByRegex(time.trim(), "\\s")))
                .collect(toUnmodifiableList());
    }

    @Override
    public List<RestrictionTime> createRestrictionTime(long wayId, List<TimeInterval> timeIntervals, String side) {
        return timeIntervals
                .stream()
                .flatMap(timeInterval -> getRestrictionTimeStream(wayId, side, timeInterval))
                .collect(toUnmodifiableList());

    }

    private Stream<RestrictionTime> getRestrictionTimeStream(long wayId, String side, TimeInterval timeInterval) {
        return timeInterval.getTimeInterval()
                .stream()
                .map(time ->
                        new RestrictionTime(wayId,
                                timeInterval.getDaysOfWeek(), timeService.getLocalTime(time[INTEGER_ZERO]),
                                timeService.getLocalTime(time[INTEGER_ONE]), side));
    }

    private TimeInterval getInterval(String[] timeInterval) {
        if (timeInterval.length == 1 && isTime(timeInterval[INTEGER_ZERO])) {
            return createTimeInterval(ALL_WEEK, timeInterval[INTEGER_ZERO]);
        }
        return createTimeInterval(timeInterval[INTEGER_ZERO], timeInterval[INTEGER_ONE]);
    }

    private TimeInterval createTimeInterval(String weekDay, String time) {
        return new TimeInterval(timeService.getDaysOfWeek(weekDay),
                timeService.getTime(time));
    }

    private boolean isTime(String elements) {
        return getSplitByRegex(elements, "-")[INTEGER_ZERO].matches(REG_EXPRESSION_TIME);
    }

    private String[] getSplitByRegex(String timeInterval, String regex) {
        return timeInterval.split(regex);
    }
}
