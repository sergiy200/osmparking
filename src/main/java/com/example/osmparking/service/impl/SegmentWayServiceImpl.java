package com.example.osmparking.service.impl;

import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.GeoJson;
import com.example.osmparking.model.GeoJsonFeature;
import com.example.osmparking.model.SegmentGeoJsonRestrictionTime;
import com.example.osmparking.model.dto.RestrictionTimeDto;
import com.example.osmparking.model.dto.RestrictionTimeIntervalDto;
import com.example.osmparking.model.dto.WayDto;
import com.example.osmparking.model.dto.WayRestrictionTime;
import com.example.osmparking.service.SegmentWayService;
import com.example.osmparking.service.WaysService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.*;

@Service
@RequiredArgsConstructor
public class SegmentWayServiceImpl implements SegmentWayService {

    private final WaysService waysService;
    private final SegmentDao segmentDao;

    @Override
    public List<SegmentGeoJsonRestrictionTime> getGeoJsonRestrictionSegment(BoundingBox boundingBox) {
        return waysService.getSegmentWithRestrictionTimes(boundingBox).stream()
                .map(segmentRestrictionTime ->
                        new SegmentGeoJsonRestrictionTime(segmentRestrictionTime.getSegmentId(),
                                createFeatures(segmentDao.getSegments(segmentRestrictionTime.getSegmentId())),
                                getCollect(segmentRestrictionTime.getRestrictionTimeDtos())))
                .collect(toUnmodifiableList());
    }

    private List<WayRestrictionTime> getCollect(Set<RestrictionTimeDto> entry) {
        return entry.stream()
                .collect(groupingBy(restriction -> new WayDto(restriction.getWayId(), restriction.getGeometry()),
                        mapping(restrictionTimeDto -> new RestrictionTimeIntervalDto(restrictionTimeDto.getDaysOfWeek(),
                                restrictionTimeDto.getFromTime(), restrictionTimeDto.getToTime(),
                                restrictionTimeDto.getSide()), toList()))).entrySet().stream()
                .map(sa -> new WayRestrictionTime(sa.getKey(), sa.getValue()))
                .collect(toUnmodifiableList());
    }

    private GeoJsonFeature createFeatures(List<GeoJson> geoJsons) {
        return geoJsons.stream()
                .map(GeoJsonFeature::new)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
