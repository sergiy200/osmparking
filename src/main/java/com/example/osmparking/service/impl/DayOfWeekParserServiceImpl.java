package com.example.osmparking.service.impl;

import com.example.osmparking.service.DayOfWeekParserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.util.Arrays;

@Slf4j
@Service
public class DayOfWeekParserServiceImpl implements DayOfWeekParserService {

    @Override
    public Integer getDayOfWeek(String days) {
        return Arrays
                .stream(DayOfWeek.values())
                .filter(dayOfWeek -> dayOfWeek.name()
                        .startsWith(days.toUpperCase()))
                .map(DayOfWeek::getValue)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Can't get day %s", days)));
    }
}
