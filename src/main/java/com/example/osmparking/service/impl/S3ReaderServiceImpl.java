package com.example.osmparking.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.example.osmparking.service.ReadCSVService;
import com.example.osmparking.service.S3ReaderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3ReaderServiceImpl implements S3ReaderService {
    private final AmazonS3 amazonS3;
    private final ReadCSVService readCSVService;

    public <T> List<T> getDataFromBucket(String bucketName, String fileName, Class<T> tClass, int lineToSkip) {
        try (Reader reader = getFileFromS3(bucketName, fileName)) {
            return readCSVService.csvReader(reader, tClass, lineToSkip);
        } catch (Exception e) {
            log.error("Error parsing file {}", fileName, e);
            throw new RuntimeException(e);
        }
    }

    private InputStreamReader getFileFromS3(String bucketName, String fileName) {
        S3Object s3Object = amazonS3.getObject(bucketName, fileName);
        return new InputStreamReader(s3Object.getObjectContent());
    }
}
