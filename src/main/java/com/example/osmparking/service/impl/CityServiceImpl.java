package com.example.osmparking.service.impl;

import com.example.osmparking.dao.CityDao;
import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.CityGeometryDto;
import com.example.osmparking.service.CityService;
import com.example.osmparking.service.S3ReaderService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private static final int LINE_TO_SKIP = 1;
    private final String bucketCities;
    private final CityDao cityDao;
    private final S3ReaderService s3ReaderService;

    public CityServiceImpl(CityDao cityDao, S3ReaderService s3ReaderService, @Value("${city.bucket.name}") String bucketCities) {
        this.cityDao = cityDao;
        this.s3ReaderService = s3ReaderService;
        this.bucketCities = bucketCities;
    }

    @Override
    public List<City> getAll() {
        return cityDao.getAll();
    }

    @Override
    public boolean saveCitiesFromCSV(String fileName) {
        List<CityDto> cities = s3ReaderService.getDataFromBucket(bucketCities, fileName, CityDto.class, LINE_TO_SKIP);
        return cityDao.saveCities(cities) == cities.size();
    }

    @Override
    public List<City> getCities(String prefix) {
        return cityDao.getCities(prefix);
    }

    @Override
    public boolean addCity(CityDto cityDto) {
        return cityDao.saveCities(List.of(cityDto)) > 0;
    }

    @Override
    public boolean delete(String prefix) {
        return cityDao.delete(prefix) > 0;
    }

    @Override
    public List<CityGeometryDto> getCityGeometry() {
        return cityDao.getCityGeometry();
    }
}
