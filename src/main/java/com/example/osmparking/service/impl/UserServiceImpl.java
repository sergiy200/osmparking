package com.example.osmparking.service.impl;

import com.example.osmparking.dao.UserDao;
import com.example.osmparking.jwt.JwtUtil;
import com.example.osmparking.model.dto.LoginDto;
import com.example.osmparking.model.dto.RegisterDto;
import com.example.osmparking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtil jwtUtil;

    @Override
    public boolean register(RegisterDto user) {
        return userDao.register(user, passwordEncoder.encode(user.getPassword())) > 0;
    }

    @Override
    public Optional<String> login(LoginDto loginDto) {
        return userDao.findByLogin(loginDto.getLogin())
                .map(user -> passwordEncoder.matches(loginDto.getPassword(), user.getPassword()) ?
                        jwtUtil.generateToken(user) : null);
    }
}
