package com.example.osmparking.service.impl;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.service.OSMApiService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.text.MessageFormat.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class OSMApiServiceImpl implements OSMApiService {

    private static final String OSM_API_URL = "http://overpass-api.de/api/interpreter";
    private static final String ELEMENTS = "elements";

    private final ObjectMapper mapper;
    private final RestTemplate restTemplate;

    @Override
    public <T> List<T> getElements(BoundingBox boundingBox, String queryPattern, Class<T> tClass) {
        String query = createQuery(boundingBox, queryPattern);
        try {
            return mapper
                    .readerForListOf(tClass)
                    .readValue(getContent(query));
        } catch (Exception e) {
            log.error("Error parsing request with BoundingBox {}", boundingBox, e);
            throw new RuntimeException(e);
        }
    }

    private JsonNode getContent(String query) {
        JsonNode elements = getJsonResponseEntity(query).get(ELEMENTS);
        if (elements.isEmpty()) {
            throw new IllegalStateException(format("Elements in response is empty {0}", query));
        }
        return elements;
    }

    private String createQuery(BoundingBox boundingBox, String query) {
        return format(query, boundingBox.getLeftLatitude(),
                boundingBox.getLeftLongitude(),
                boundingBox.getRightLatitude(),
                boundingBox.getRightLongitude());
    }

    private JsonNode getJsonResponseEntity(String query) {
        return restTemplate
                .exchange(OSM_API_URL, HttpMethod.POST, new HttpEntity<>(query), JsonNode.class).getBody();
    }
}
