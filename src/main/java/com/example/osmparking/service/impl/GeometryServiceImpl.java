package com.example.osmparking.service.impl;

import com.example.osmparking.service.GeometryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.operation.buffer.BufferParameters;
import org.springframework.stereotype.Service;

import static java.text.MessageFormat.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class GeometryServiceImpl implements GeometryService {

    private static final WKTReader WKT_READER = new WKTReader();
    private static final double BUFFER_DISTANCE = 0.0002;

    @Override
    public String buffered(String geometry) {
        log.info("Buffer geometry and get bounding box {}", geometry);
        return buffer(getGeometry(geometry), BUFFER_DISTANCE).getEnvelope().toString();
    }

    @Override
    public boolean matchIntersectionByPercent(String geometry, String intersectGeometry, double percent) {
        log.info("Match geometries {}, {} to {} percent", geometry, intersectGeometry, percent);
        return (buffer(getGeometry(geometry), BUFFER_DISTANCE)
                .intersection(buffer(getGeometry(intersectGeometry), BUFFER_DISTANCE)).getArea()
                / (buffer(getGeometry(geometry), BUFFER_DISTANCE)).getArea()) >= percent;
    }

    public Geometry buffer(Geometry restrictionTimeDto, double distance) {
        return restrictionTimeDto.buffer(distance, BufferParameters.CAP_FLAT, 0);
    }

    private Geometry getGeometry(String geometry) {
        try {
            return WKT_READER.read(geometry);
        } catch (ParseException e) {
            log.error("Error parse to geometry {}", geometry);
            throw new IllegalArgumentException(format("Error parse to geometry {0} {1}", geometry, e));
        }
    }
}
