package com.example.osmparking.service.impl;

import com.example.osmparking.dao.ParkingTimeDao;
import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.MatchedSegmentRestrictionTime;
import com.example.osmparking.model.dto.RestrictionTimeDto;
import com.example.osmparking.model.dto.SegmentRestrictionTime;
import com.example.osmparking.service.GeometryService;
import com.example.osmparking.service.WaysService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Service
@RequiredArgsConstructor
public class WaysServiceImpl implements WaysService {

    private static final double PERCENT_INTERSECTION = 0.7;
    private final GeometryService geometryService;
    private final ParkingTimeDao parkingTimeDao;
    private final SegmentDao segmentDao;

    @Override
    public List<SegmentRestrictionTime> getSegmentWithRestrictionTimes(BoundingBox boundingBox) {
        return getSegmentRestrictionTimeMap(boundingBox)
                .entrySet().stream()
                .map(entry -> new SegmentRestrictionTime(entry.getKey(), entry.getValue()))
                .collect(toUnmodifiableList());
    }

    private Map<Long, Set<RestrictionTimeDto>> getSegmentRestrictionTimeMap(BoundingBox boundingBox) {
        return parkingTimeDao.getRestrictionTime(boundingBox).stream()
                .flatMap(this::getMatchedSegmentRestrictionTimeStream)
                .collect(groupingBy(MatchedSegmentRestrictionTime::getSegmentId,
                        mapping(MatchedSegmentRestrictionTime::getRestrictionTimeDto, toSet())));
    }

    private Stream<MatchedSegmentRestrictionTime> getMatchedSegmentRestrictionTimeStream(RestrictionTimeDto restrict) {
        return segmentDao.getIntersectedSegmentGeometries(geometryService.buffered(restrict.getGeometry())).stream()
                .filter(segment -> geometryService.matchIntersectionByPercent(restrict.getGeometry(), segment.getWay().toString(), PERCENT_INTERSECTION))
                .map(seg -> new MatchedSegmentRestrictionTime(seg.getSegmentId(), restrict));
    }
}
