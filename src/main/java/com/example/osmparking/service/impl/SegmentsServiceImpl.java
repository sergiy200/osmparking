package com.example.osmparking.service.impl;

import com.example.osmparking.dao.ParkingTimeDao;
import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.*;
import com.example.osmparking.model.dto.*;
import com.example.osmparking.service.OSMDataProvider;
import com.example.osmparking.service.S3ReaderService;
import com.example.osmparking.service.SegmentsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.example.osmparking.model.utils.QueryUtils.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toUnmodifiableList;

@Slf4j
@Service
public class SegmentsServiceImpl implements SegmentsService {

    private static final int LINE_TO_SKIP = 0;

    private final String bucketSegments;
    private final S3ReaderService s3ReaderService;
    private final SegmentDao segmentDao;
    private final ParkingTimeDao parkingTimeDao;
    private final OSMDataProvider osmDataProvider;

    public SegmentsServiceImpl(S3ReaderService s3ReaderService, SegmentDao segmentDao,
                               @Value("${segment.bucket.name}") String bucketSegments,
                               ParkingTimeDao parkingTimeDao, OSMDataProvider osmDataProvider) {
        this.s3ReaderService = s3ReaderService;
        this.segmentDao = segmentDao;
        this.bucketSegments = bucketSegments;
        this.parkingTimeDao = parkingTimeDao;
        this.osmDataProvider = osmDataProvider;
    }

    @Override
    public List<Segment> getSegmentsFromCSV(String fileName) {
        return s3ReaderService.getDataFromBucket(bucketSegments, fileName, Segment.class, LINE_TO_SKIP);
    }

    @Override
    public boolean save(List<Segment> segments) {
        return segmentDao.save(segments) == segments.size();
    }

    @Override
    public void setCityPrefixToSegments(List<SegmentWayGeometryDto> segments, List<CityGeometryDto> cities) {
        List<SegmentCityPrefixDto> segmentCityPrefixDto = segments
                .stream()
                .map(segment -> getIntersectedCity(cities, segment))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
        log.info("Updated lines {}", segmentDao.updateSegmentCities(segmentCityPrefixDto));
    }

    @Override
    public List<SegmentCityPrefixDto> getCityPrefixWithIntersectedSegment() {
        return segmentDao.getCityPrefixWithIntersectedSegments();
    }

    @Override
    public boolean updateSegmentsCities(List<SegmentCityPrefixDto> segmentCityDto) {
        return segmentDao.updateSegmentCities(segmentCityDto) == segmentCityDto.size();
    }

    @Override
    public boolean updateSegment(UpdateSegmentDTO updateSegmentDTO) {
        return segmentDao.updateSegment(updateSegmentDTO.getSegmentId(), updateSegmentDTO) > 0;
    }

    @Override
    public List<SegmentWayGeometryDto> getSegmentGeometry() {
        return segmentDao.getSegmentGeometry();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveNoParkingSegments(BoundingBox boundingBox) {
        List<NoParkingSegment> noParkingSegments = osmDataProvider.getNoParkingSegments(boundingBox, GET_NO_PARKING);
        segmentDao.deleteParkingWays(boundingBox);
        return noParkingSegments.size() == segmentDao.saveParkingWays(noParkingSegments);
    }

    @Override
    public List<NoParkingSegment> getNoParking() {
        return segmentDao.getNoParking();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveRestrictionTime(BoundingBox boundingBox) {
        parkingTimeDao.deleteRestrictionTime(boundingBox);
        return parkingTimeDao.saveRestrictionTime(osmDataProvider.getRestrictionTimes(boundingBox, GET_NO_PARKING_TIME));
    }

    @Override
    public GeoJsonFeatureCollection getWays(BoundingBox boundingBox) {
        return new GeoJsonFeatureCollection(createFeatures(segmentDao.getWays(boundingBox)));
    }

    @Override
    public GeoJsonFeatureCollection getNoParkingMapBox(BoundingBox boundingBox) {
        return new GeoJsonFeatureCollection(createFeatures(segmentDao.getNoParkingMapBox(boundingBox)));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveAllWays(BoundingBox boundingBox) {
        List<Ways> ways = osmDataProvider.getWays(boundingBox, GET_ALL_WAYS);
        segmentDao.deleteWays(boundingBox);
        return segmentDao.saveWays(ways);
    }

    private List<GeoJsonFeature> createFeatures(List<GeoJson> geoJsons) {
        return geoJsons.stream()
                .map(GeoJsonFeature::new)
                .collect(toUnmodifiableList());
    }

    private Optional<SegmentCityPrefixDto> getIntersectedCity(List<CityGeometryDto> cities, SegmentWayGeometryDto segment) {
        return cities.stream()
                .filter(city -> segment.getWay().intersects(city.getRegion()))
                .findFirst()
                .map(cityGeometryDto -> new SegmentCityPrefixDto(segment.getSegmentId(), cityGeometryDto.getPrefixCity()));
    }
}
