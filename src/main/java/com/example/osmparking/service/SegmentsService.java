package com.example.osmparking.service;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.GeoJsonFeatureCollection;
import com.example.osmparking.model.Segment;
import com.example.osmparking.model.dto.*;

import java.util.List;

public interface SegmentsService {
    List<Segment> getSegmentsFromCSV(String fileName);

    boolean save(List<Segment> segments);

    void setCityPrefixToSegments(List<SegmentWayGeometryDto> segments, List<CityGeometryDto> cities);

    List<SegmentCityPrefixDto> getCityPrefixWithIntersectedSegment();

    boolean updateSegmentsCities(List<SegmentCityPrefixDto> segmentCityDto);

    boolean updateSegment(UpdateSegmentDTO updateSegmentDTO);

    List<SegmentWayGeometryDto> getSegmentGeometry();

    boolean saveNoParkingSegments(BoundingBox boundingBox);

    List<NoParkingSegment> getNoParking();

    int saveRestrictionTime(BoundingBox boundingBox);

    GeoJsonFeatureCollection getWays(BoundingBox boundingBox);

    GeoJsonFeatureCollection getNoParkingMapBox(BoundingBox boundingBox);

    int saveAllWays(BoundingBox boundingBox);
}
