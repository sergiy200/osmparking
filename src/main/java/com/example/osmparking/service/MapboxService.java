package com.example.osmparking.service;

public interface MapboxService {
    String getToken();
}
