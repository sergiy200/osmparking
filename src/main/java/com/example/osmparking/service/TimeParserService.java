package com.example.osmparking.service;

import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.TimeInterval;

import java.util.List;

public interface TimeParserService {

   List<TimeInterval> getTimeInterval(String timeInterval);

   List<RestrictionTime> createRestrictionTime(long wayId, List<TimeInterval> timeIntervals, String side);
}
