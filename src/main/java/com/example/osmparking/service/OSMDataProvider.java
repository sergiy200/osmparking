package com.example.osmparking.service;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.Ways;
import com.example.osmparking.model.dto.NoParkingSegment;

import java.util.List;

public interface OSMDataProvider {

    List<RestrictionTime> getRestrictionTimes(BoundingBox boundingBox, String query);

    List<NoParkingSegment> getNoParkingSegments(BoundingBox boundingBox, String query);

    List<Ways> getWays(BoundingBox boundingBox, String getAllWays);
}
