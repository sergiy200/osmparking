package com.example.osmparking.properties;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Setter
@Component
@ConfigurationProperties(prefix = "spring.web.security.url")
public class WebSecurityProperties {
    private List<String> permitAllPaths;
    private List<String> adminPaths;

    public String[] getPermitAllPaths() {
        return permitAllPaths.toArray(String[]::new);
    }

    public String[] getAdminPaths() {
        return adminPaths.toArray(String[]::new);
    }
}
