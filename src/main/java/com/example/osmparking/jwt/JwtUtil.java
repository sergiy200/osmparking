package com.example.osmparking.jwt;

import com.example.osmparking.model.User;
import com.example.osmparking.service.TimeService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
public class JwtUtil {

    private static final String TOKEN_PREFIX = "Bearer ";
    private final TimeService timeService;
    private final String secretKey;
    private final long sessionTime;

    public JwtUtil(TimeService timeService,
                   @Value("${JWT_SECRET}") String secretKey,
                   @Value("${JWT_SESSIONTIME}") long sessionTime) {
        this.secretKey = secretKey;
        this.sessionTime = sessionTime;
        this.timeService = timeService;
    }

    public String generateToken(User user) {
        return TOKEN_PREFIX + createToken(Map.of(AUTHORIZATION, user.getRole()), user.getLogin());
    }

    private String createToken(Map<String, Object> claims, String subject) {
        Date currentDate = timeService.getCurrentTimeMillis();
        return Jwts.builder()
                .setSubject(subject)
                .addClaims(claims)
                .setIssuedAt(currentDate)
                .setExpiration(expireTimeFromNow(currentDate))
                .signWith(SignatureAlgorithm.HS256, secretKey).compact();
    }

    private Date expireTimeFromNow(Date currentDate) {
        return new Date(currentDate.getTime() + sessionTime);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    public String getUsername(String token) {
        return extractAllClaims(token).getSubject();
    }

    public String getAuthorities(String token) {
        return String.valueOf(extractAllClaims(token).get(AUTHORIZATION));
    }
}