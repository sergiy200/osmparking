package com.example.osmparking.config;

import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.boot.autoconfigure.jooq.SpringTransactionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class JooqConfig {

    @Bean
    public PlatformTransactionManager transactionManager(DataSource operationsDataSource){
        return new DataSourceTransactionManager(operationsDataSource);
    }

    @Bean
    public ConnectionProvider connectionProvider(DataSource operationsDataSource){
        return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(operationsDataSource));
    }

    @Bean
    public DSLContext dslContext(DataSource operationsDataSource,
                                 SpringTransactionProvider transactionProvider,
                                 ConnectionProvider connectionProvider) {
        org.jooq.Configuration configuration =  new DefaultConfiguration()
                .derive(operationsDataSource)
                .derive(transactionProvider)
                .derive(connectionProvider)
                .derive(SQLDialect.POSTGRES);
        return DSL.using(configuration);
    }
}