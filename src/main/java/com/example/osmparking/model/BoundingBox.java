package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class BoundingBox {

    double leftLatitude, leftLongitude;
    double rightLatitude, rightLongitude;

    @JsonCreator
    public BoundingBox(@JsonProperty("leftLatitude") @JsonAlias("minlat") double leftLatitude,
                       @JsonProperty("leftLongitude") @JsonAlias("minlon")double leftLongitude,
                       @JsonProperty("rightLatitude")@JsonAlias("maxlat") double rightLatitude,
                       @JsonProperty("rightLongitude") @JsonAlias("maxlon")double rightLongitude) {
        this.leftLatitude = leftLatitude;
        this.leftLongitude = leftLongitude;
        this.rightLatitude = rightLatitude;
        this.rightLongitude = rightLongitude;
    }
}
