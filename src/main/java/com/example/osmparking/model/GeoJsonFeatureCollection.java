package com.example.osmparking.model;

import lombok.Value;

import java.util.List;

@Value
public class GeoJsonFeatureCollection {
    String type = "FeatureCollection";
    List<GeoJsonFeature> features;
}
