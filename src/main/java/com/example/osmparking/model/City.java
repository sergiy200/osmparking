package com.example.osmparking.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class City {

    int id;
    String nameCity;
    String prefixCity;
    BoundingBox boundingBox;
    String region;
}