package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class GeoJsonFeature {
    String type = "Feature";
    GeoJson geometry;

    @JsonCreator
    public GeoJsonFeature(@JsonProperty("geometry") GeoJson geometry) {
        this.geometry = geometry;
    }
}
