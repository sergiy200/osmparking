package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class GeoJson {

    String type;
    List<List<Double>> coordinates;

    @JsonCreator
    public GeoJson(@JsonProperty("type") String type, @JsonProperty("coordinates") List<List<Double>> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }
}
