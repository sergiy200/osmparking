package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Value
public class OSMElement {
    long id;
    long uid;
    String type;
    String user;
    double lat;
    double lon;
    Date timestamp;
    int version;
    long changeSet;
    List<Double> nodes;
    LinkedHashMap tags;
    List<LinkedHashMap> members;

    @JsonCreator
    public OSMElement(
            @JsonProperty("id") long id,
            @JsonProperty("uid") long uid,
            @JsonProperty("type") String type,
            @JsonProperty("user") String user,
            @JsonProperty("lat") double lat,
            @JsonProperty("lon") double lon,
            @JsonProperty("timestamp") Date timestamp,
            @JsonProperty("version") int version,
            @JsonProperty("changeset") long changeSet,
            @JsonProperty("nodes") List<Double> nodes,
            @JsonProperty("tags") LinkedHashMap tags,
            @JsonProperty("members") List<LinkedHashMap> members) {
        this.id = id;
        this.uid = uid;
        this.type = type;
        this.user = user;
        this.lat = lat;
        this.lon = lon;
        this.timestamp = timestamp;
        this.version = version;
        this.changeSet = changeSet;
        this.nodes = nodes;
        this.tags = tags;
        this.members = members;
    }
}
