package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.LinkedHashMap;
import java.util.List;

@Value
public class OSMWay {
    long id;
    String type;
    BoundingBox bounds;
    List<Double> nodes;
    List<Point> geometry;
    LinkedHashMap<String, String> tags;

    @JsonCreator
    public OSMWay(
            @JsonProperty("id") long id,
            @JsonProperty("type") String type,
            @JsonProperty("bounds") BoundingBox bounds,
            @JsonProperty("nodes") List<Double> nodes,
            @JsonProperty("geometry") List<Point> geometry,
            @JsonProperty("tags") LinkedHashMap<String, String> tags) {
        this.id = id;
        this.type = type;
        this.bounds = bounds;
        this.nodes = nodes;
        this.geometry = geometry;
        this.tags = tags;
    }
}
