package com.example.osmparking.model;

import lombok.Value;

import java.time.LocalTime;

@Value
public class RestrictionTime {
    long wayId;
    Integer[] daysOfWeek;
    LocalTime fromTime;
    LocalTime toTime;
    String side;
}
