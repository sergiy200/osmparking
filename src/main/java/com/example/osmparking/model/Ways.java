package com.example.osmparking.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ways {
    long id;
    String geometry;
    String highway;

    @JsonCreator
    public Ways(@JsonProperty("id") long id,
                @JsonProperty("geometry") String geometry,
                @JsonProperty("tag:highway") String highway) {
        this.id = id;
        this.geometry = geometry;
        this.highway = highway;
    }
}
