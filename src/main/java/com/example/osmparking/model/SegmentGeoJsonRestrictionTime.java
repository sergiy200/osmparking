package com.example.osmparking.model;

import com.example.osmparking.model.dto.WayRestrictionTime;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class SegmentGeoJsonRestrictionTime {
    long segmentId;
    GeoJsonFeature geoJsonFeature;
    List<WayRestrictionTime> wayRestrictionTimes;

    @JsonCreator
    public SegmentGeoJsonRestrictionTime(@JsonProperty("segmentId") long segmentId,
                                         @JsonProperty("geoJsonFeature") GeoJsonFeature geoJsonFeature,
                                         @JsonProperty("wayRestrictionTimes") List<WayRestrictionTime> wayRestrictionTimes) {
        this.segmentId = segmentId;
        this.geoJsonFeature = geoJsonFeature;
        this.wayRestrictionTimes = wayRestrictionTimes;
    }
}
