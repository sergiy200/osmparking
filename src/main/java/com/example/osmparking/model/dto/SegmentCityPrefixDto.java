package com.example.osmparking.model.dto;

import lombok.Value;

@Value
public class SegmentCityPrefixDto {
    long segmentId;
    String prefixCity;
}
