package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoParkingTags {

    String highway;
    String parkingConditionRight;
    String parkingConditionLeft;
    String parkingConditionBoth;
    String parkingLaneLeft;
    String parkingLaneRight;
    String parkingLaneBoth;

    @JsonCreator
    public NoParkingTags(
            @JsonProperty("highway") String highway,
            @JsonProperty("parking:condition:right") String parkingConditionRight,
            @JsonProperty("parking:condition:left") String parkingConditionLeft,
            @JsonProperty("parking:condition:both") String parkingConditionBoth,
            @JsonProperty("parking:lane:left") String parkingLaneLeft,
            @JsonProperty("parking:lane:right") String parkingLaneRight,
            @JsonProperty("parking:lane:both") String parkingLaneBoth) {
        this.highway = highway;
        this.parkingConditionRight = parkingConditionRight;
        this.parkingConditionLeft = parkingConditionLeft;
        this.parkingConditionBoth = parkingConditionBoth;
        this.parkingLaneLeft = parkingLaneLeft;
        this.parkingLaneRight = parkingLaneRight;
        this.parkingLaneBoth = parkingLaneBoth;
    }
}
