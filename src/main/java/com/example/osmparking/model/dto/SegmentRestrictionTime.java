package com.example.osmparking.model.dto;

import lombok.Value;

import java.util.Set;

@Value
public class SegmentRestrictionTime {
    long segmentId;
    Set<RestrictionTimeDto> restrictionTimeDtos;
}
