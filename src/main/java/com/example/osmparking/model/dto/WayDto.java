package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class WayDto {
    long wayId;
    String geometry;

    @JsonCreator
    public WayDto(@JsonProperty("wayId") long wayId, @JsonProperty("geometry") String geometry) {
        this.wayId = wayId;
        this.geometry = geometry;
    }
}
