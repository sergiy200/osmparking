package com.example.osmparking.model.dto;

import lombok.Value;
import org.locationtech.jts.geom.Geometry;

@Value
public class SegmentWayGeometryDto {
    long segmentId;
    Geometry way;
}
