package com.example.osmparking.model.dto;

import lombok.Value;

import java.util.List;

@Value
public class RestrictionTimeDto {
    long wayId;
    String geometry;
    List<Integer> daysOfWeek;
    String fromTime;
    String toTime;
    String side;
}
