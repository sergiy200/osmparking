package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class UpdateSegmentDTO {
    long segmentId;
    int fromNodeId;
    int toNodeId;
    String points;
    String way;
    String roadType;
    int idRegion;
    String highway;
    String streetName;
    String link1;
    String point;
    String cityPrefix;

    @JsonCreator
    public UpdateSegmentDTO(
            @JsonProperty("segmentId") long segmentId,
            @JsonProperty("fromNodeId") int fromNodeId,
            @JsonProperty("toNodeId") int toNodeId,
            @JsonProperty("points") String points,
            @JsonProperty("way") String way,
            @JsonProperty("roadType") String roadType,
            @JsonProperty("idRegion") int idRegion,
            @JsonProperty("highway") String highway,
            @JsonProperty("streetName") String streetName,
            @JsonProperty("link1") String link1,
            @JsonProperty("point") String point,
            @JsonProperty("city") String cityPrefix) {
        this.segmentId = segmentId;
        this.fromNodeId = fromNodeId;
        this.toNodeId = toNodeId;
        this.points = points;
        this.way = way;
        this.roadType = roadType;
        this.idRegion = idRegion;
        this.highway = highway;
        this.streetName = streetName;
        this.link1 = link1;
        this.point = point;
        this.cityPrefix = cityPrefix;
    }
}
