package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class RegisterDto {

    public String userName;
    public String login;
    public String password;

    @JsonCreator
    public RegisterDto(@JsonProperty("userName") String userName,
                       @JsonProperty("login") String login,
                       @JsonProperty("password") String password) {
        this.userName = userName;
        this.login = login;
        this.password = password;
    }
}
