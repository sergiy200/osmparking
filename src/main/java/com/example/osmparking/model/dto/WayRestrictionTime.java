package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class WayRestrictionTime {
    WayDto wayDto;
    List<RestrictionTimeIntervalDto> restrictionTimeIntervalDto;

    @JsonCreator
    public WayRestrictionTime(@JsonProperty("wayDto") WayDto wayDto,
                              @JsonProperty("restrictionTimeIntervalDto") List<RestrictionTimeIntervalDto> restrictionTimeIntervalDto) {
        this.wayDto = wayDto;
        this.restrictionTimeIntervalDto = restrictionTimeIntervalDto;
    }
}
