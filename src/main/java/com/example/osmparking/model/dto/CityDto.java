package com.example.osmparking.model.dto;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CityDto {

    @CsvBindByPosition(position = 1)
    String nameCity;
    @CsvBindByPosition(position = 2)
    String prefixCity;
    @CsvBindByPosition(position = 3)
    double leftLatitude;
    @CsvBindByPosition(position = 4)
    double leftLongitude;
    @CsvBindByPosition(position = 5)
    double rightLatitude;
    @CsvBindByPosition(position = 6)
    double rightLongitude;
    @CsvBindByPosition(position = 7)
    String region;
}
