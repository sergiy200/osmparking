package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class ParkingTimeWay {

    long wayId;
    ParkingTimeTags tags;

    @JsonCreator
    public ParkingTimeWay(
            @JsonProperty("id") long wayId,
            @JsonProperty("tags") ParkingTimeTags tags) {
        this.wayId = wayId;
        this.tags = tags;
    }
}
