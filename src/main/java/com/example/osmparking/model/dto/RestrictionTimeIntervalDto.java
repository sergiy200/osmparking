package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class RestrictionTimeIntervalDto {
    List<Integer> daysOfWeek;
    String fromTime;
    String toTime;
    String side;

    @JsonCreator
    public RestrictionTimeIntervalDto(@JsonProperty("daysOfWeek") List<Integer> daysOfWeek,
                                      @JsonProperty("fromTime") String fromTime,
                                      @JsonProperty("toTime") String toTime,
                                      @JsonProperty("side") String side) {
        this.daysOfWeek = daysOfWeek;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.side = side;
    }
}
