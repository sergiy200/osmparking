package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParkingTimeTags {
    String parkingConditionRightTime;
    String parkingConditionLeftTime;
    String parkingConditionBothTime;

    @JsonCreator
    public ParkingTimeTags(
            @JsonProperty("parking:condition:right:time_interval") String parkingConditionRight,
            @JsonProperty("parking:condition:left:time_interval") String parkingConditionLeft,
            @JsonProperty("parking:condition:both:time_interval") String parkingConditionBoth) {
        this.parkingConditionRightTime = parkingConditionRight;
        this.parkingConditionLeftTime = parkingConditionLeft;
        this.parkingConditionBothTime = parkingConditionBoth;
    }
}
