package com.example.osmparking.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class NoParkingSegment {
    long id;
    String geometry;
    NoParkingTags tags;

    @JsonCreator
    public NoParkingSegment(
            @JsonProperty("id") long id,
            @JsonProperty("geometry") String geometry,
            @JsonProperty("tags") NoParkingTags tags) {
        this.id = id;
        this.geometry = geometry;
        this.tags = tags;
    }
}
