package com.example.osmparking.model;

import com.example.osmparking.model.dto.RestrictionTimeDto;
import lombok.Value;

@Value
public class MatchedSegmentRestrictionTime {
    long segmentId;
    RestrictionTimeDto restrictionTimeDto;
}
