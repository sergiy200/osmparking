package com.example.osmparking.model.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class QueryUtils {
    public static String GET_ALL_WAYS = "[out:json][bbox:{0},{1},{2},{3}];\n" +
            "way" +
            "[\"highway\"~\"motorway|motorway_link|trunk|trunk_link|primary|primary_link|secondary|" +
            "secondary_link|tertiary|tertiary_link|service|residential|unclassified|living_street|pedestrian|track|construction\"];\n" +
            "out geom;";

    public static String GET_NO_PARKING = "[out:json]\n" +
            "[bbox:{0},{1},{2},{3}];\n" +
            "(\n" +
            "way[\"parking:lane:left\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            "way[\"parking:lane:right\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            "way[\"parking:lane:both\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            "way[\"parking:condition:right\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            "way[\"parking:condition:both\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            "way[\"parking:condition:left\"~\"no_parking|no_stopping|no\"][highway!~\"footway\"][highway];\n" +
            ");\n" +
            "out geom;";

    public static String GET_NO_PARKING_TIME = "[out:json]\n" +
            "[bbox:{0},{1},{2},{3}];\n" +
            "(\n" +
            "way[\"parking:condition:left\"~\"no_parking|no_stopping|no\"][\"parking:condition:left:time_interval\"];\n" +
            "way[\"parking:condition:both\"~\"no_parking|no_stopping|no\"][\"parking:condition:both:time_interval\"];\n" +
            "way[\"parking:condition:right\"~\"no_parking|no_stopping|no\"][\"parking:condition:right:time_interval\"];\n" +
            "way[\"parking:lane:left\"~\"no_parking|no_stopping|no\"][\"parking:condition:left:time_interval\"];\n" +
            "way[\"parking:lane:both\"~\"no_parking|no_stopping|no\"][\"parking:condition:both:time_interval\"];\n" +
            "way[\"parking:lane:right\"~\"no_parking|no_stopping|no\"][\"parking:condition:right:time_interval\"];\n" +
            ");\n" +
            "out geom;";
}
