package com.example.osmparking.model.enums;

import com.example.osmparking.model.dto.ParkingTimeTags;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.cglib.core.internal.Function;

import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum Side {
    RIGHT("right", parkingTimeTags -> Optional.ofNullable(parkingTimeTags.getParkingConditionRightTime())),
    LEFT("left", parkingTimeTags -> Optional.ofNullable(parkingTimeTags.getParkingConditionLeftTime())),
    BOTH("both", parkingTimeTags -> Optional.ofNullable(parkingTimeTags.getParkingConditionBothTime()));
    private final String name;
    private final Function<ParkingTimeTags, Optional<String>> parkingTime;
}
