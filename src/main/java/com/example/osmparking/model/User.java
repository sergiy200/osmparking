package com.example.osmparking.model;

import lombok.Value;

@Value
public class User {

    int id;
    String userName;
    String login;
    String password;
    String role;
}
