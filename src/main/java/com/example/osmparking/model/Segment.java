package com.example.osmparking.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Segment {
    @CsvBindByPosition(position = 0)
    long segmentId;
    @CsvBindByPosition(position = 1)
    int fromNodeId;
    @CsvBindByPosition(position = 2)
    int toNodeId;
    @CsvBindByPosition(position = 3)
    String fromNodeCoords;
    @CsvBindByPosition(position = 4)
    String toNodeCoords;
    @CsvBindByPosition(position = 5)
    String points;
    @CsvBindByPosition(position = 6)
    String way;
    @CsvBindByPosition(position = 7)
    String roadType;
    @CsvBindByPosition(position = 9)
    int idRegion;
    @CsvBindByPosition(position = 8)
    String highway;
    @CsvBindByPosition(position = 10)
    String streetName;
    @CsvBindByPosition(position = 11)
    String link1;
    @CsvBindByPosition(position = 12)
    String link2;
    @CsvBindByPosition(position = 13)
    String point;
    String city;
}
