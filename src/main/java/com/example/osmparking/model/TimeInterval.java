package com.example.osmparking.model;

import lombok.Value;

import java.util.List;

@Value
public class TimeInterval {

    Integer[] daysOfWeek;
    List<String[]> timeInterval;
}
