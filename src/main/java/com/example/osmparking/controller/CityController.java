package com.example.osmparking.controller;

import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.UpdateSegmentDTO;
import com.example.osmparking.service.CitySegmentService;
import com.example.osmparking.service.CityService;
import com.example.osmparking.service.SegmentsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cities")
public class CityController {

    private final CityService cityService;
    private final CitySegmentService citySegmentService;
    private final SegmentsService segmentsService;

    @GetMapping
    public ResponseEntity<List<City>> getAll() {
        return ResponseEntity.ok(cityService.getAll());
    }

    @PostMapping("/{fileName}")
    public ResponseEntity<Object> saveCities(@PathVariable String fileName) {
        return Optional.of(cityService.saveCitiesFromCSV(fileName))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.CREATED).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @GetMapping("/{prefix}")
    public ResponseEntity<List<City>> getCitiesByPrefix(@PathVariable String prefix) {
        List<City> cities = cityService.getCities(prefix);
        if (cities.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(cities);
    }

    @DeleteMapping("/{prefix}")
    public ResponseEntity<Object> delete(@PathVariable String prefix) {
        return Optional.of(cityService.delete(prefix))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.OK).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PostMapping("/city")
    public ResponseEntity<Object> addCity(@RequestBody CityDto cityDto) {
        return Optional.of(cityService.addCity(cityDto))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.CREATED).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PostMapping("/segments/{file}")
    public ResponseEntity<Object> saveSegments(@PathVariable String file) {
        return Optional.of(citySegmentService.save(file))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.CREATED).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PutMapping("/segments")
    public ResponseEntity<Void> updateSegment() {
        citySegmentService.updateCityPrefixToSegmentsByJTS();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/prefix")
    public ResponseEntity<Object> updateCityPrefixToSegments() {
        return Optional.of(citySegmentService.updateCityPrefixByPostGis())
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.CREATED).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PutMapping("/segment")
    public ResponseEntity<Object> updateSegment(@RequestBody UpdateSegmentDTO updateSegmentDTO) {
        return Optional.of(segmentsService.updateSegment(updateSegmentDTO))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.OK).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }
}
