package com.example.osmparking.controller;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.GeoJsonFeatureCollection;
import com.example.osmparking.model.OSMElement;
import com.example.osmparking.model.SegmentGeoJsonRestrictionTime;
import com.example.osmparking.model.dto.NoParkingSegment;
import com.example.osmparking.model.dto.SegmentRestrictionTime;
import com.example.osmparking.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.osmparking.model.utils.QueryUtils.GET_ALL_WAYS;

@RestController
@RequestMapping("segments")
@RequiredArgsConstructor
public class OSMSegmentController {
    private static final String MAPBOX_TOKEN = "Mapbox-Token";
    private final MapboxService mapboxService;
    private final SegmentsService segmentsService;
    private final OSMApiService osmApiService;
    private final WaysService waysService;
    private final SegmentWayService segmentWayService;

    @PostMapping("noparking")
    public ResponseEntity<Void> saveNoParkingSegments(@RequestBody BoundingBox boundingBox) {
        if (segmentsService.saveNoParkingSegments(boundingBox)) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("noparking")
    public ResponseEntity<List<NoParkingSegment>> getNoParking() {
        return ResponseEntity.ok(segmentsService.getNoParking());
    }

    @GetMapping("elements")
    public ResponseEntity<List<OSMElement>> getElements(BoundingBox boundingBox) {
        return ResponseEntity.ok(osmApiService.getElements(boundingBox, GET_ALL_WAYS, OSMElement.class));
    }

    @PostMapping("noparking-time")
    public ResponseEntity<Integer> saveNoParkingTime(@RequestBody BoundingBox boundingBox) {
        return ResponseEntity.ok(segmentsService.saveRestrictionTime(boundingBox));
    }

    @GetMapping("way")
    public ResponseEntity<GeoJsonFeatureCollection> getWaysGeoJson(BoundingBox boundingBox) {
        return ResponseEntity.ok(segmentsService.getWays(boundingBox));
    }

    @GetMapping("access-token")
    public ResponseEntity<Void> getToken() {
        return ResponseEntity.status(HttpStatus.OK).header(MAPBOX_TOKEN, mapboxService.getToken()).build();
    }

    @PostMapping("all-ways")
    public ResponseEntity<Integer> saveAllWays(@RequestBody BoundingBox boundingBox) {
        return ResponseEntity.ok(segmentsService.saveAllWays(boundingBox));
    }

    @GetMapping("noparking-mapbox")
    public ResponseEntity<GeoJsonFeatureCollection> getNoParkingMapBox(BoundingBox boundingBox) {
        return ResponseEntity.ok(segmentsService.getNoParkingMapBox(boundingBox));
    }

    @GetMapping("restrict-ways")
    public ResponseEntity<List<SegmentRestrictionTime>> getRestrictWays(BoundingBox boundingBox) {
        return ResponseEntity.ok(waysService.getSegmentWithRestrictionTimes(boundingBox));
    }

    @GetMapping("restrict")
    public ResponseEntity<List<SegmentGeoJsonRestrictionTime>> getRes(BoundingBox boundingBox) {
        return ResponseEntity.ok(segmentWayService.getGeoJsonRestrictionSegment(boundingBox));
    }
}
