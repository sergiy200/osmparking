package com.example.osmparking.controller;

import com.example.osmparking.model.dto.LoginDto;
import com.example.osmparking.model.dto.RegisterDto;
import com.example.osmparking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;

    @PostMapping("/registration")
    public ResponseEntity<Object> registration(@RequestBody RegisterDto registerDto) {
        return Optional.of(userService.register(registerDto))
                .filter(data -> data)
                .map(data -> ResponseEntity.status(HttpStatus.CREATED).build())
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PostMapping("/login")
    public ResponseEntity<Void> login(@RequestBody LoginDto loginDto) {
        return userService.login(loginDto)
                .map(userToken -> ResponseEntity.status(HttpStatus.OK).header(AUTHORIZATION, userToken).build())
                .orElse(new ResponseEntity(HttpStatus.BAD_REQUEST));
    }
}
