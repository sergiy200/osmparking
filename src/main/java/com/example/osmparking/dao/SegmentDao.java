package com.example.osmparking.dao;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.GeoJson;
import com.example.osmparking.model.Segment;
import com.example.osmparking.model.Ways;
import com.example.osmparking.model.dto.NoParkingSegment;
import com.example.osmparking.model.dto.SegmentCityPrefixDto;
import com.example.osmparking.model.dto.SegmentWayGeometryDto;
import com.example.osmparking.model.dto.UpdateSegmentDTO;

import java.util.List;

public interface SegmentDao {
    int save(List<Segment> segments);

    List<SegmentCityPrefixDto> getCityPrefixWithIntersectedSegments();

    int updateSegmentCities(List<SegmentCityPrefixDto> segmentCityDto);

    int updateSegment(long segmentId, UpdateSegmentDTO updateSegmentDTO);

    List<SegmentWayGeometryDto> getSegmentGeometry();

    List<SegmentWayGeometryDto> getIntersectedSegmentGeometries(String geometry);

    int saveWays(List<Ways> ways);

    int saveParkingWays(List<NoParkingSegment> segments);

    List<NoParkingSegment> getNoParking();

    void deleteWays(BoundingBox boundingBox);

    void deleteParkingWays(BoundingBox boundingBox);

    List<GeoJson> getWays(BoundingBox boundingBox);

    List<GeoJson> getNoParkingMapBox(BoundingBox boundingBox);

    List<GeoJson> getSegments(long segmentsId);
}
