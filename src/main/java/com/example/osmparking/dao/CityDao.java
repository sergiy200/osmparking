package com.example.osmparking.dao;

import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.CityGeometryDto;

import java.util.List;

public interface CityDao {

    int saveCities(List<CityDto> cities);

    List<City> getAll();

    List<City> getCities(String prefix);

    int delete(String prefix);

    List<CityGeometryDto> getCityGeometry();
}
