package com.example.osmparking.dao.impl;

import com.example.osmparking.dao.UserDao;
import com.example.osmparking.model.User;
import com.example.osmparking.model.dto.RegisterDto;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static com.example.test.jooq.public_.tables.Users.USERS;

@Repository
@RequiredArgsConstructor
public class UserDaoImpl implements UserDao {

    private static final String ROLE_USER = "USER";
    private final DSLContext dslContext;

    @Override
    public int register(RegisterDto user, String password) {
        return dslContext
                .insertInto(USERS, USERS.USER_NAME, USERS.LOGIN, USERS.PASSWORD, USERS.USER_ROLE)
                .values(user.getUserName(), user.getLogin(), password, ROLE_USER)
                .execute();
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return Optional.ofNullable(dslContext
                .selectFrom(USERS)
                .where(USERS.LOGIN.eq(login))
                .fetchAnyInto(User.class));
    }
}
