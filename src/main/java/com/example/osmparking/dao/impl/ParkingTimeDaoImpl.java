package com.example.osmparking.dao.impl;

import com.example.osmparking.dao.ParkingTimeDao;
import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.dto.RestrictionTimeDto;
import com.example.test.jooq.public_.tables.records.RestrictionTimeRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.InsertSetMoreStep;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.osmparking.dao.fields.Fields.*;
import static com.example.test.jooq.public_.Tables.RESTRICTION_TIME;
import static com.example.test.jooq.public_.tables.Ways.WAYS;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.jooq.impl.DSL.insertInto;

@Slf4j
@Repository
@RequiredArgsConstructor
public class ParkingTimeDaoImpl implements ParkingTimeDao {

    private final DSLContext dslContext;

    @Override
    public int saveRestrictionTime(List<RestrictionTime> parkingTimeWay) {
        return dslContext
                .batch(prepareInsertParkingTime(parkingTimeWay))
                .execute()
                .length;
    }

    @Override
    public void deleteRestrictionTime(BoundingBox boundingBox) {
        dslContext
                .deleteFrom(RESTRICTION_TIME)
                .using(WAYS)
                .where(RESTRICTION_TIME.WAY_ID.eq(WAYS.WAY_ID)
                        .and(stContains(getBboxField(boundingBox), WAYS.WAY)))
                .execute();
    }

    @Override
    public List<RestrictionTimeDto> getRestrictionTime(BoundingBox boundingBox) {
        return dslContext
                .select(RESTRICTION_TIME.WAY_ID,
                        stAsText(WAYS.WAY),
                        RESTRICTION_TIME.WEEKS_INTERVAL,
                        RESTRICTION_TIME.TIME_FROM,
                        RESTRICTION_TIME.TIME_TO,
                        RESTRICTION_TIME.SIDE)
                .from(RESTRICTION_TIME)
                .innerJoin(WAYS).on(WAYS.WAY_ID.eq(RESTRICTION_TIME.WAY_ID))
                .where(stContains(getBboxField(boundingBox), WAYS.WAY))
                .fetchInto(RestrictionTimeDto.class);
    }

    private List<InsertSetMoreStep<RestrictionTimeRecord>> prepareInsertParkingTime(List<RestrictionTime> restrictionTimes) {
        return restrictionTimes.stream()
                .map(this::insert)
                .collect(toUnmodifiableList());
    }

    private InsertSetMoreStep<RestrictionTimeRecord> insert(RestrictionTime restrictionTime) {
        return insertInto(RESTRICTION_TIME)
                .set(RESTRICTION_TIME.WAY_ID, restrictionTime.getWayId())
                .set(RESTRICTION_TIME.WEEKS_INTERVAL, restrictionTime.getDaysOfWeek())
                .set(RESTRICTION_TIME.TIME_FROM, restrictionTime.getFromTime())
                .set(RESTRICTION_TIME.TIME_TO, restrictionTime.getToTime())
                .set(RESTRICTION_TIME.SIDE, restrictionTime.getSide());
    }
}
