package com.example.osmparking.dao.impl;

import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.GeoJson;
import com.example.osmparking.model.Segment;
import com.example.osmparking.model.Ways;
import com.example.osmparking.model.dto.*;
import com.example.test.jooq.public_.tables.records.ParkingWaysRecord;
import com.example.test.jooq.public_.tables.records.SegmentsRecord;
import com.example.test.jooq.public_.tables.records.WaysRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jooq.*;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.osmparking.dao.fields.Fields.*;
import static com.example.test.jooq.public_.Tables.PARKING_WAYS;
import static com.example.test.jooq.public_.Tables.SEGMENTS;
import static com.example.test.jooq.public_.tables.City.CITY;
import static com.example.test.jooq.public_.tables.Ways.WAYS;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.jooq.impl.DSL.*;
import static org.jooq.tools.Convert.convert;

@Slf4j
@Repository
@RequiredArgsConstructor
public class SegmentDaoImpl implements SegmentDao {

    private static final String WAY_GEO_JSON_WAYS = "way_geojson_ways";
    private static final String NO_PARKING = "no_parking";
    private static final String NO_STOPPING = "no_stopping";
    private static final String NO = "no";
    private final DSLContext dslContext;
    private final ObjectMapper objectMapper;
    private final WKTReader wktReader = new WKTReader();

    @Override
    public int save(List<Segment> segments) {
        return dslContext.batch(prepareInsert(segments))
                .execute().length;
    }

    @Override
    public List<SegmentCityPrefixDto> getCityPrefixWithIntersectedSegments() {
        return dslContext
                .selectDistinct(SEGMENTS.SEGMENT_ID, CITY.PREFIX_CITY)
                .from(SEGMENTS, CITY)
                .where(stIntersects(CITY.REGION, SEGMENTS.WAY))
                .fetchInto(SegmentCityPrefixDto.class);
    }

    @Override
    public int updateSegmentCities(List<SegmentCityPrefixDto> segmentCityDto) {
        return dslContext
                .batch(prepareUpdate(segmentCityDto))
                .execute().length;
    }

    @Override
    public int updateSegment(long segmentId, UpdateSegmentDTO updateSegmentDTO) {
        return dslContext
                .update(SEGMENTS)
                .set(SEGMENTS.FROM_NODE_ID, updateSegmentDTO.getFromNodeId())
                .set(SEGMENTS.TO_NODE_ID, updateSegmentDTO.getToNodeId())
                .set(SEGMENTS.POINTS, convert(updateSegmentDTO.getPoints(), JSON.class))
                .set(SEGMENTS.WAY, setToGeometry(updateSegmentDTO.getWay()))
                .set(SEGMENTS.ROAD_TYPE, updateSegmentDTO.getRoadType())
                .set(SEGMENTS.HIGHWAY, updateSegmentDTO.getHighway())
                .set(SEGMENTS.IN_REGION, updateSegmentDTO.getIdRegion())
                .set(SEGMENTS.STREET_NAME, updateSegmentDTO.getStreetName())
                .set(SEGMENTS.LINK1, convert(updateSegmentDTO.getLink1(), JSON.class))
                .set(SEGMENTS.POINT, setToGeometry(updateSegmentDTO.getPoint()))
                .set(SEGMENTS.CITY, updateSegmentDTO.getCityPrefix())
                .where(SEGMENTS.SEGMENT_ID.eq(segmentId))
                .execute();
    }

    @Override
    public List<SegmentWayGeometryDto> getSegmentGeometry() {
        return dslContext
                .select(SEGMENTS.SEGMENT_ID,
                        stAsText(SEGMENTS.WAY))
                .from(SEGMENTS)
                .fetch(this::getCityGeometryDto);
    }

    @Override
    public int saveWays(List<Ways> ways) {
        return dslContext
                .batch(prepareInsertWay(ways))
                .execute()
                .length;
    }

    @Override
    public int saveParkingWays(List<NoParkingSegment> segments) {
        return dslContext
                .batch(prepareInsertOSMWay(segments))
                .execute()
                .length;
    }

    @Override
    public void deleteWays(BoundingBox boundingBox) {
        dslContext
                .deleteFrom(WAYS)
                .where(stContains(getBboxField(boundingBox), WAYS.WAY))
                .execute();
    }

    @Override
    public void deleteParkingWays(BoundingBox boundingBox) {
        dslContext
                .deleteFrom(PARKING_WAYS)
                .using(WAYS)
                .where(PARKING_WAYS.WAY_ID.eq(WAYS.WAY_ID)
                        .and(stContains(getBboxField(boundingBox), WAYS.WAY)))
                .execute();
    }

    @Override
    public List<GeoJson> getWays(BoundingBox boundingBox) {
        return dslContext
                .select(stGeoJson(WAYS.WAY).as(WAY_GEO_JSON_WAYS))
                .from(WAYS)
                .where(stContains(getBboxField(boundingBox), WAYS.WAY))
                .fetch(this::getGeoJson);
    }

    @Override
    public List<NoParkingSegment> getNoParking() {
        return dslContext
                .select(WAYS.WAY_ID,
                        stAsText(WAYS.WAY),
                        WAYS.HIGHWAY,
                        PARKING_WAYS.PARKING_CONDITION_RIGHT,
                        PARKING_WAYS.PARKING_CONDITION_LEFT,
                        PARKING_WAYS.PARKING_CONDITION_BOTH,
                        PARKING_WAYS.PARKING_LANE_LEFT,
                        PARKING_WAYS.PARKING_LANE_RIGHT,
                        PARKING_WAYS.PARKING_LANE_BOTH)
                .from(WAYS)
                .innerJoin(PARKING_WAYS).on(PARKING_WAYS.WAY_ID.eq(WAYS.WAY_ID))
                .fetch(this::getNoParkingSegment);

    }

    @Override
    public List<GeoJson> getNoParkingMapBox(BoundingBox boundingBox) {
        return dslContext
                .select(stGeoJson(WAYS.WAY).as(WAY_GEO_JSON_WAYS))
                .from(WAYS)
                .innerJoin(PARKING_WAYS).on(PARKING_WAYS.WAY_ID.eq(WAYS.WAY_ID))
                .where(PARKING_WAYS.PARKING_LANE_RIGHT.eq(NO_PARKING)
                        .or(PARKING_WAYS.PARKING_LANE_BOTH.eq(NO_PARKING))
                        .or(PARKING_WAYS.PARKING_LANE_LEFT.eq(NO_PARKING))
                        .or(PARKING_WAYS.PARKING_LANE_BOTH.eq(NO_STOPPING))
                        .or(PARKING_WAYS.PARKING_LANE_LEFT.eq(NO_STOPPING))
                        .or(PARKING_WAYS.PARKING_LANE_RIGHT.eq(NO_STOPPING))
                        .or(PARKING_WAYS.PARKING_LANE_BOTH.eq(NO))
                        .or(PARKING_WAYS.PARKING_LANE_LEFT.eq(NO))
                        .or(PARKING_WAYS.PARKING_LANE_RIGHT.eq(NO))
                        .and(stContains(getBboxField(boundingBox), WAYS.WAY)))
                .fetch(this::getGeoJson);
    }

    @Override
    public List<SegmentWayGeometryDto> getIntersectedSegmentGeometries(String geometry) {
        return dslContext
                .select(SEGMENTS.SEGMENT_ID,
                        stAsText(SEGMENTS.WAY))
                .from(SEGMENTS)
                .where(stIntersects(SEGMENTS.WAY, getGeometrySRID(geometry)))
                .fetch(this::getCityGeometryDto);
    }

    @Override
    public List<GeoJson> getSegments(long segmentsId) {
        return dslContext.select(stGeoJson(SEGMENTS.WAY).as(WAY_GEO_JSON_WAYS))
                .from(SEGMENTS)
                .where(SEGMENTS.SEGMENT_ID.eq(segmentsId))
                .fetch(this::getGeoJson);
    }

    private GeoJson getGeoJson(Record1<String> geoJson) {
        try {
            return objectMapper.readValue(geoJson.get(WAY_GEO_JSON_WAYS, String.class), GeoJson.class);
        } catch (Exception e) {
            log.error("Error parsing result {}", geoJson);
            throw new RuntimeException(format("Error parsing result {0} {1}", geoJson, e));
        }
    }

    private List<InsertSetMoreStep<ParkingWaysRecord>> prepareInsertOSMWay(List<NoParkingSegment> segments) {
        return segments.stream()
                .map(segment -> insertInto(PARKING_WAYS)
                        .set(PARKING_WAYS.WAY_ID, segment.getId())
                        .set(PARKING_WAYS.PARKING_CONDITION_BOTH, segment.getTags().getParkingConditionBoth())
                        .set(PARKING_WAYS.PARKING_CONDITION_LEFT, segment.getTags().getParkingConditionLeft())
                        .set(PARKING_WAYS.PARKING_CONDITION_RIGHT, segment.getTags().getParkingConditionRight())
                        .set(PARKING_WAYS.PARKING_LANE_BOTH, segment.getTags().getParkingLaneBoth())
                        .set(PARKING_WAYS.PARKING_LANE_LEFT, segment.getTags().getParkingLaneLeft())
                        .set(PARKING_WAYS.PARKING_LANE_RIGHT, segment.getTags().getParkingLaneRight()))
                .collect(toUnmodifiableList());
    }

    private List<InsertSetMoreStep<WaysRecord>> prepareInsertWay(List<Ways> segments) {
        return segments.stream()
                .map(segment -> insertInto(WAYS)
                        .set(WAYS.WAY_ID, segment.getId())
                        .set(WAYS.WAY, segment.getGeometry())
                        .set(WAYS.HIGHWAY, segment.getHighway()))
                .collect(toUnmodifiableList());
    }

    private NoParkingSegment getNoParkingSegment(Record9<Long, ?, String, String, String, String, String, String, String> record) {
        try {
            return new NoParkingSegment(record.get(WAYS.WAY_ID),
                    record.get(stAsText(WAYS.WAY), String.class),
                    new NoParkingTags(
                            record.get(WAYS.HIGHWAY),
                            record.get(PARKING_WAYS.PARKING_CONDITION_RIGHT),
                            record.get(PARKING_WAYS.PARKING_CONDITION_LEFT),
                            record.get(PARKING_WAYS.PARKING_CONDITION_BOTH),
                            record.get(PARKING_WAYS.PARKING_LANE_LEFT),
                            record.get(PARKING_WAYS.PARKING_LANE_RIGHT),
                            record.get(PARKING_WAYS.PARKING_LANE_BOTH)));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @NonNull
    private List<InsertSetMoreStep<SegmentsRecord>> prepareInsert(List<Segment> segments) {
        return segments
                .stream()
                .map(segment -> insertInto(SEGMENTS)
                        .set(SEGMENTS.SEGMENT_ID, segment.getSegmentId())
                        .set(SEGMENTS.FROM_NODE_ID, segment.getFromNodeId())
                        .set(SEGMENTS.TO_NODE_ID, segment.getToNodeId())
                        .set(SEGMENTS.FROM_NODE_COORDS, convert(segment.getFromNodeCoords(), JSON.class))
                        .set(SEGMENTS.TO_NODE_COORDS, convert(segment.getToNodeCoords(), JSON.class))
                        .set(SEGMENTS.POINTS, convert(segment.getPoints(), JSON.class))
                        .set(SEGMENTS.WAY, setToGeometry(segment.getWay()))
                        .set(SEGMENTS.ROAD_TYPE, segment.getRoadType())
                        .set(SEGMENTS.HIGHWAY, segment.getHighway())
                        .set(SEGMENTS.IN_REGION, segment.getIdRegion())
                        .set(SEGMENTS.STREET_NAME, segment.getStreetName())
                        .set(SEGMENTS.LINK1, convert(segment.getLink1(), JSON.class))
                        .set(SEGMENTS.LINK2, convert(segment.getLink2(), JSON.class))
                        .set(SEGMENTS.POINT, setToGeometry(segment.getPoint()))
                        .set(SEGMENTS.CITY, EMPTY)).collect(toUnmodifiableList());
    }

    private List<UpdateConditionStep<SegmentsRecord>> prepareUpdate(List<SegmentCityPrefixDto> segmentCityDto) {
        return segmentCityDto
                .stream()
                .map(segment ->
                        update(SEGMENTS)
                                .set(SEGMENTS.CITY, segment.getPrefixCity())
                                .where(SEGMENTS.SEGMENT_ID.eq(segment.getSegmentId())))
                .collect(toUnmodifiableList());
    }

    private SegmentWayGeometryDto getCityGeometryDto(Record2<Long, ?> record) {
        try {
            return new SegmentWayGeometryDto(record.get(SEGMENTS.SEGMENT_ID), wktReader.read(record.get(stAsText(SEGMENTS.WAY), String.class)));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
