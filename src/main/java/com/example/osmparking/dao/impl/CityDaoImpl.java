package com.example.osmparking.dao.impl;

import com.example.test.jooq.public_.tables.records.CityRecord;
import com.example.osmparking.dao.CityDao;
import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.CityGeometryDto;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.InsertSetMoreStep;
import org.jooq.Record2;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.test.jooq.public_.tables.City.CITY;
import static com.example.osmparking.dao.fields.Fields.stAsText;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.jooq.impl.DSL.insertInto;

@Repository
@RequiredArgsConstructor
public class CityDaoImpl implements CityDao {

    private final DSLContext dslContext;
    private final WKTReader wktReader = new WKTReader();

    @Override
    public int saveCities(List<CityDto> cities) {
        return dslContext.batch(prepareInsert(cities))
                .execute().length;
    }

    @Override
    public List<City> getAll() {
        return dslContext
                .selectFrom(CITY)
                .orderBy(CITY.ID)
                .fetch(this::getCity);
    }

    @Override
    public int delete(String prefix) {
        return dslContext
                .deleteFrom(CITY)
                .where(CITY.PREFIX_CITY.eq(prefix))
                .execute();
    }

    @Override
    public List<City> getCities(String prefix) {
        return dslContext
                .selectFrom(CITY)
                .where(CITY.PREFIX_CITY.eq(prefix))
                .fetch(this::getCity);
    }

    @Override
    public List<CityGeometryDto> getCityGeometry() {
        return dslContext
                .select(CITY.PREFIX_CITY,
                        stAsText(CITY.REGION))
                .from(CITY)
                .fetch(this::getCityGeometryDto);
    }

    private List<InsertSetMoreStep<CityRecord>> prepareInsert(List<CityDto> cities) {
        return cities.stream().map(city -> insertInto(CITY)
                .set(CITY.NAME_CITY, city.getNameCity())
                .set(CITY.PREFIX_CITY, city.getPrefixCity())
                .set(CITY.LEFT_LATITUDE, city.getLeftLatitude())
                .set(CITY.RIGHT_LATITUDE, city.getRightLatitude())
                .set(CITY.LEFT_LONGITUDE, city.getLeftLongitude())
                .set(CITY.RIGHT_LONGITUDE, city.getRightLongitude())
                .set(CITY.REGION, city.getRegion())
        ).collect(toUnmodifiableList());
    }

    private City getCity(CityRecord cityRecord) {
        return new City(cityRecord.getId(),
                cityRecord.getNameCity(),
                cityRecord.getPrefixCity(),
                new BoundingBox(cityRecord.getLeftLatitude(), cityRecord.getLeftLongitude(), cityRecord.getRightLatitude(), cityRecord.getRightLongitude()),
                cityRecord.getRegion().toString());
    }

    private CityGeometryDto getCityGeometryDto(Record2<String, ?> record) {
        try {
            return new CityGeometryDto(record.get(CITY.PREFIX_CITY), wktReader.read(record.get(stAsText(CITY.REGION), String.class)));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
