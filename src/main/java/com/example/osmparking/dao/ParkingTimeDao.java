package com.example.osmparking.dao;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.dto.RestrictionTimeDto;

import java.util.List;

public interface ParkingTimeDao {

    void deleteRestrictionTime(BoundingBox boundingBox);

    int saveRestrictionTime(List<RestrictionTime> parkingTimeWay);

    List<RestrictionTimeDto> getRestrictionTime(BoundingBox boundingBox);
}
