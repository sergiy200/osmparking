package com.example.osmparking.dao.fields;

import com.example.osmparking.model.BoundingBox;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jooq.Field;
import org.jooq.QueryPart;

import static org.jooq.impl.DSL.field;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fields {
    public static Field<?> stAsText(QueryPart... parts) {
        return field("st_asText({0})", parts);
    }

    public static Object setToGeometry(Object... bindings) {
        return field("{0}::geometry", bindings);
    }

    public static Field<Boolean> stIntersects(QueryPart... parts) {
        return field("ST_Intersects({0}, {1})", boolean.class, parts);
    }

    public static Field<Object> getBboxField(BoundingBox boundingBox) {
        return field("'BOX(" +
                boundingBox.getLeftLongitude() + " " + boundingBox.getLeftLatitude() + "," +
                boundingBox.getRightLongitude() + " " + boundingBox.getRightLatitude() + ")'::box2d"
        );
    }

    public static Field<Boolean> stContains(QueryPart... parts) {
        return field("ST_Contains({0}, {1})", boolean.class, parts);
    }

    public static Field<String> stGeoJson(Object... geometry) {
        return field("ST_asGeoJson({0})", String.class, geometry);
    }

    public static Field<Object> getGeometrySRID(String geometry) {
        return field("st_geometryFromText({0},4326)", geometry);
    }

}
