package com.example.osmparking.dao;

import com.example.osmparking.model.User;
import com.example.osmparking.model.dto.RegisterDto;

import java.util.Optional;


public interface UserDao {
    int register(RegisterDto user, String password);

    Optional<User> findByLogin(String login);
}
