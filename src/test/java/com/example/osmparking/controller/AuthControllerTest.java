package com.example.osmparking.controller;

import com.example.osmparking.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;
import java.util.UUID;

import static com.example.osmparking.UserTestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthControllerTest {

    private static final String JWT_TOKEN = UUID.randomUUID().toString();
    private static final String LOGIN_PATH = "/login";
    private static final String REGISTRATION_PATH = "/registration";

    private final UserService userService = mock(UserService.class);
    private final AuthController authController = new AuthController(userService);
    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(authController).build();

    @Test
    public void registrationTest() throws Exception {
        when(userService.register(registerUser())).thenReturn(true);
        mockMvc.perform(post(REGISTRATION_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userRegRequest))
                .andExpect(status().isCreated());
    }

    @Test
    public void registrationFailInsertTest() throws Exception {
        when(userService.register(registerUser())).thenReturn(false);
        mockMvc.perform(post(REGISTRATION_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userRegRequest))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void loginTest() throws Exception {
        when(userService.login(authorizationData())).thenReturn(Optional.of( JWT_TOKEN));
        String response = mockMvc.perform(post(LOGIN_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userLoginRequest))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getHeader(AUTHORIZATION);
        assertThat(response, is(JWT_TOKEN));
    }

    @Test
    public void loginFailTest() throws Exception {
        when(userService.login(authorizationData())).thenReturn(Optional.empty());
        mockMvc.perform(post(LOGIN_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userLoginRequest))
                .andExpect(status().isBadRequest());
    }
}
