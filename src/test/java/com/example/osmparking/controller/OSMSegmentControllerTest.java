package com.example.osmparking.controller;

import com.example.osmparking.model.OSMElement;
import com.example.osmparking.model.SegmentGeoJsonRestrictionTime;
import com.example.osmparking.model.dto.NoParkingSegment;
import com.example.osmparking.service.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static com.example.osmparking.model.utils.QueryUtils.GET_ALL_WAYS;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OSMSegmentControllerTest {

    private static final String SEGMENT = "/segments";
    private static final String ELEMENTS = "/elements";
    private static final String NO_PARKING = "/noparking";
    private static final String RESTRICT = "/restrict";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String MAPBOX_TOKEN_HEADER = "Mapbox-Token";
    private static final String TOKEN = "token";
    private static final String ACCESS_TOKEN = "/access-token";

    private final SegmentsService segmentsService = mock(SegmentsService.class);
    private final OSMApiService osmApiService = mock(OSMApiService.class);
    private final MapboxService mapboxService = mock(MapboxService.class);
    private final WaysService waysService = mock(WaysService.class);
    private final SegmentWayService segmentWayService = mock(SegmentWayService.class);
    private final OSMSegmentController segmentController = new OSMSegmentController(mapboxService, segmentsService, osmApiService, waysService, segmentWayService);
    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(segmentController).build();

    @Test
    public void getElementsTest() throws Exception {
        List<OSMElement> elements = getOSMElement();
        when(osmApiService.getElements(boundingBox, GET_ALL_WAYS, OSMElement.class)).thenReturn(elements);
        String mvcResult = mockMvc.perform(get(SEGMENT + ELEMENTS)
                        .params(bboxRequestParams)
                )
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse().getContentAsString();
        assertThat(OBJECT_MAPPER.readValue(mvcResult, new TypeReference<List<OSMElement>>() {
        }), containsInAnyOrder(elements.get(INTEGER_ZERO), elements.get(INTEGER_ONE)));
    }

    @Test
    public void setNoParkingSegments() throws Exception {
        when(segmentsService.saveNoParkingSegments(boundingBox)).thenReturn(true);
        mockMvc.perform(post(SEGMENT + NO_PARKING)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(boundingBox)))
                .andExpect(status().isOk());
        verify(segmentsService).saveNoParkingSegments(boundingBox);
    }

    @Test
    public void getNoParking() throws Exception {
        when(segmentsService.getNoParking()).thenReturn(getNoParkingSegment());
        String mvcResult = mockMvc.perform(get(SEGMENT + NO_PARKING)
                        .params(bboxRequestParams))
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn()
                .getResponse().getContentAsString();
        assertThat(OBJECT_MAPPER.readValue(mvcResult, new TypeReference<List<NoParkingSegment>>() {
        }), containsInAnyOrder(getNoParkingSegment().get(INTEGER_ZERO), getNoParkingSegment().get(INTEGER_ONE)));
    }

    @Test
    public void getMapboxToken() throws Exception {
        when(mapboxService.getToken()).thenReturn(TOKEN);
        String response = mockMvc.perform(get(SEGMENT + ACCESS_TOKEN))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getHeader(MAPBOX_TOKEN_HEADER);
        assertThat(response, is(TOKEN));
    }

    @Test
    public void getRestrict() throws Exception {
        when(segmentWayService.getGeoJsonRestrictionSegment(boundingBox)).thenReturn(segmentGeoJsonRestrictionTime());
        String response = mockMvc.perform(get(SEGMENT + RESTRICT)
                        .params(bboxRequestParams))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        assertThat(OBJECT_MAPPER.readValue(response, new TypeReference<List<SegmentGeoJsonRestrictionTime>>() {
        }), containsInAnyOrder(segmentGeoJsonRestrictionTime().get(0), segmentGeoJsonRestrictionTime().get(1)));
    }
}
