package com.example.osmparking.controller;

import com.example.osmparking.model.City;
import com.example.osmparking.service.CitySegmentService;
import com.example.osmparking.service.CityService;
import com.example.osmparking.service.SegmentsService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.*;
import static com.example.osmparking.SegmentTestDataGenerator.updateSegmentDTO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CityControllerTest {

    private static final String CITIES = "/cities";
    private static final String PREFIX = "prefix";
    private static final String FILE_NAME = "fileName";
    private static final String SEGMENT = "/segments";
    private static final String PATH_TO_REQUEST_CITY = "src/main/resources/static/cityRequest.json";
    private static final String PATH_TO_REQUEST_SEGMENT = "src/main/resources/static/updateSegmentRequest.json";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final CityService cityService = Mockito.mock(CityService.class);
    private final CitySegmentService citySegmentService = Mockito.mock(CitySegmentService.class);
    private final SegmentsService segmentsService = Mockito.mock(SegmentsService.class);
    private final CityController cityController = new CityController(cityService, citySegmentService, segmentsService);
    private final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(cityController).build();

    @Test
    public void getAll() throws Exception {
        List<City> cities = getListOfCities();
        when(cityService.getAll()).thenReturn(cities);
        String mvcResult = mockMvc.perform(get(CITIES))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andReturn()
                .getResponse().getContentAsString();
        assertThat(OBJECT_MAPPER.readValue(mvcResult, new TypeReference<>() {
        }), is(cities));
    }

    @Test
    public void setCityTest() throws Exception {
        mockMvc.perform(post(CITIES + "/" + FILE_NAME));
        verify(cityService).saveCitiesFromCSV(FILE_NAME);
    }

    @Test
    public void getCitiesTest() throws Exception {
        List<City> cities = getListOfCities();
        when(cityService.getCities(PREFIX)).thenReturn(cities);
        String mvcResult = mockMvc.perform(get(CITIES + "/" + PREFIX))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertThat(OBJECT_MAPPER.readValue(mvcResult, new TypeReference<>() {
        }), is(cities));
    }

    @Test
    public void getEmptyCitiesListTest() throws Exception {
        when(cityService.getCities(PREFIX)).thenReturn(List.of());
        mockMvc.perform(get(CITIES + "/" + PREFIX))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void addCity() throws Exception {
        when(cityService.addCity(cityDto())).thenReturn(true);
        mockMvc.perform(post(CITIES + "/city")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(cityDto())))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteCityByPrefix() throws Exception {
        when(cityService.delete(PREFIX)).thenReturn(true);
        mockMvc.perform(delete(CITIES + "/" + PREFIX))
                .andExpect(status().isOk());
    }

    @Test
    public void saveSegmentsTest() throws Exception {
        when(citySegmentService.save(FILE_NAME)).thenReturn(true);
        mockMvc.perform(post(CITIES + SEGMENT + "/" + FILE_NAME))
                .andExpect(status().isCreated());
    }

    @Test
    public void setPrefixTest() throws Exception {
        when(citySegmentService.updateCityPrefixByPostGis()).thenReturn(true);
        mockMvc.perform(put(CITIES + "/" + PREFIX))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateSegmentsCityPrefixTest() throws Exception {
        mockMvc.perform(put(CITIES + SEGMENT))
                .andExpect(status().isOk());
        verify(citySegmentService).updateCityPrefixToSegmentsByJTS();
    }

    @Test
    public void updateSegmentTest() throws Exception {
        when(segmentsService.updateSegment(updateSegmentDTO)).thenReturn(true);
        mockMvc.perform(put(CITIES + "/segment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getRequestBodyFromFile(PATH_TO_REQUEST_SEGMENT)))
                .andExpect(status().isOk());
    }
}
