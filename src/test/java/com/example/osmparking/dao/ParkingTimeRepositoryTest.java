package com.example.osmparking.dao;

import com.example.osmparking.dao.impl.ParkingTimeDaoImpl;
import com.example.osmparking.dao.impl.SegmentDaoImpl;
import com.example.osmparking.model.RestrictionTime;
import com.example.osmparking.model.Ways;
import com.example.osmparking.model.dto.NoParkingSegment;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static com.example.osmparking.dao.TestDslContextSupplier.getConnection;
import static com.example.test.jooq.public_.Tables.RESTRICTION_TIME;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("test")
public class ParkingTimeRepositoryTest {
    private final DSLContext dslContext = getConnection();

    private final ParkingTimeDao parkingTimeDao = new ParkingTimeDaoImpl(dslContext);

    private final SegmentDao segmentDao = new SegmentDaoImpl(dslContext, new ObjectMapper());

    @BeforeEach
    void beforeTest() {
        List<NoParkingSegment> noParkingSegment = getNoParkingSegment();
        List<Ways> ways = getWays(noParkingSegment);
        segmentDao.saveWays(ways);
        segmentDao.saveParkingWays(noParkingSegment);
        List<RestrictionTime> parkingTimeWays = getParkingTimeWays();
        parkingTimeDao.saveRestrictionTime(parkingTimeWays);
    }

    @AfterEach
    void afterTest() {
        segmentDao.deleteParkingWays(boundingBox);
        parkingTimeDao.deleteRestrictionTime(boundingBox);
        segmentDao.deleteWays(boundingBox);
    }

    @Test
    public void deleteTest() {
        int countElementAfterDelete = 0;
        parkingTimeDao.deleteRestrictionTime(boundingBox);
        assertThat(getCountParkingTimeElements(), is(countElementAfterDelete));
    }

    @Test
    public void saveParkingTimeTest() {
        int afterSave = getCountParkingTimeElements() + parkingTimeDao.saveRestrictionTime(getParkingTimeWays());
        assertThat(getCountParkingTimeElements(), is(afterSave));
    }

    @Test
    public void getRestrictionTime() {
        assertThat(parkingTimeDao.getRestrictionTime(boundingBox),
                containsInAnyOrder(getRestrictionTimeDtoList().get(0), getRestrictionTimeDtoList().get(1)));
    }

    private List<Ways> getWays(List<NoParkingSegment> noParkingSegment) {
        return noParkingSegment.stream()
                .map(seg -> new Ways(seg.getId(), seg.getGeometry(), seg.getTags().getHighway()))
                .collect(toUnmodifiableList());
    }

    private int getCountParkingTimeElements() {
        return dslContext.selectFrom(RESTRICTION_TIME).execute();
    }
}
