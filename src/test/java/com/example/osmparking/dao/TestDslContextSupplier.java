package com.example.osmparking.dao;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestDslContextSupplier {
    private static final String DB_URL = "jdbc:postgresql://localhost:5431/OSMgisTest";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "root";

    /**
     * Should be closed in the @AfterClass method
     */
    public static DSLContext getConnection() {
        return DSL.using(DB_URL, DB_USER, DB_PASSWORD);
    }
}