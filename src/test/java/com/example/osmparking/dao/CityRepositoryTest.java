package com.example.osmparking.dao;

import com.example.osmparking.dao.impl.CityDaoImpl;
import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import org.jooq.DSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static com.example.osmparking.dao.TestDslContextSupplier.getConnection;
import static com.example.test.jooq.public_.tables.City.CITY;
import static com.example.osmparking.CityTestDataGenerator.getCityGeometryDto;
import static com.example.osmparking.CityTestDataGenerator.getListOfCitiesDtoFromDB;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ActiveProfiles("test")
public class CityRepositoryTest {

    private static final List<CityDto> listCitiesDTO = getListOfCitiesDtoFromDB();
    private final DSLContext dslContext = getConnection();
    private final CityDao cityDao = new CityDaoImpl(dslContext);

    @BeforeEach
    void beforeTest() {
        cityDao.saveCities(listCitiesDTO);
    }

    @AfterEach
    void delete() {
        dslContext.deleteFrom(CITY).execute();
    }

    @Test
    public void testGetAll() {
        cityDao.getAll();
        int listSize = 2;
        assertThat(cityDao.getAll().size(), is(listSize));
    }

    @Test
    public void testSaveCities() {
        int countSavedElements = 2;
        List<City> cityBeforeInsert = getCities();
        cityDao.saveCities(listCitiesDTO);
        List<City> cityAfterInsert = getCities();
        assertThat(cityDao.saveCities(listCitiesDTO), is(countSavedElements));
        assertThat(cityBeforeInsert.size(), is(cityAfterInsert.size() - listCitiesDTO.size()));
    }

    @Test
    public void testGetCity() {
        int secondElement = 1;
        CityDto cityDto = listCitiesDTO.get(secondElement);
        assertThat(cityDao.getCities(cityDto.getPrefixCity()).get(0).getPrefixCity(), is(cityDto.getPrefixCity()));
    }

    @Test
    public void testDelete() {
        int secondElementAndDeletedCount = 1;
        CityDto cityDto = listCitiesDTO.get(secondElementAndDeletedCount);
        assertThat(cityDao.delete(cityDto.getPrefixCity()), is(secondElementAndDeletedCount));
        List<City> city = getCityByPrefix(cityDto.getPrefixCity());
        assertThat(city, is(empty()));
    }

    @Test
    public void testGetPolygons() {
        int firstElement = 0;
        int secondElement = 1;
        assertThat(cityDao.getCityGeometry(), containsInAnyOrder(getCityGeometryDto().get(firstElement), getCityGeometryDto().get(secondElement)));
    }

    private List<City> getCities() {
        return dslContext
                .selectFrom(CITY)
                .fetchInto(City.class);
    }

    private List<City> getCityByPrefix(String cityPrefix) {
        return dslContext.selectFrom(CITY).where(CITY.PREFIX_CITY.eq(cityPrefix)).fetchInto(City.class);
    }
}
