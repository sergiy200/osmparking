package com.example.osmparking.dao;

import com.example.osmparking.SegmentTestDataGenerator;
import com.example.osmparking.dao.impl.CityDaoImpl;
import com.example.osmparking.dao.impl.SegmentDaoImpl;
import com.example.osmparking.model.Segment;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static com.example.osmparking.dao.TestDslContextSupplier.getConnection;
import static com.example.test.jooq.public_.tables.City.CITY;
import static com.example.test.jooq.public_.tables.Segments.SEGMENTS;
import static com.example.osmparking.CityTestDataGenerator.getListOfCitiesDtoFromDB;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("test")
public class SegmentsRepositoryTest {

    private static final int FIRST_ELEMENT = 0;
    private final DSLContext dslContext = getConnection();

    private final SegmentDao segmentDao = new SegmentDaoImpl(dslContext,new ObjectMapper());

    private final CityDao cityDao = new CityDaoImpl(dslContext);

    @AfterEach
    public void after() {
        dslContext.deleteFrom(SEGMENTS).execute();
    }

    @Test
    public void getSegmentGeometryTest() {
        segmentDao.save(SegmentTestDataGenerator.getSegment());
        assertThat(segmentDao.getSegmentGeometry(), containsInRelativeOrder(segmentWayGeometryDto().get(FIRST_ELEMENT)));
    }

    @Test
    public void getCityPrefixWithIntersectedSegments() {
        segmentDao.save(SegmentTestDataGenerator.getSegment());
        cityDao.saveCities(getListOfCitiesDtoFromDB());
        assertThat(segmentDao.getCityPrefixWithIntersectedSegments(), containsInRelativeOrder(getIntersectedCityPrefixDto().get(FIRST_ELEMENT)));
        dslContext.deleteFrom(CITY).execute();
    }

    @Test
    public void saveTest() {
        int countElements = 2;
        assertThat(segmentDao.save(SegmentTestDataGenerator.getSegment()), is(countElements));
        List<Segment> segmentAfterInsert = getSegments();
        assertThat(segmentAfterInsert.size(), is(countElements));
    }

    @Test
    public void updateSegmentCitiesTest() {
        int savedElement = 1;
        segmentDao.save(SegmentTestDataGenerator.getSegment());
        assertThat(segmentDao.updateSegmentCities(getIntersectedCityPrefixDto()), is(savedElement));
        Optional<Segment> optional = getSegmentById(getIntersectedCityPrefixDto().get(FIRST_ELEMENT).getSegmentId());
        assertThat(optional.isPresent(), is(true));
        assertThat(optional.get().getCity(), is(getIntersectedCityPrefixDto().get(FIRST_ELEMENT).getPrefixCity()));
    }

    @Test
    public void updateSegment() {
        segmentDao.save(SegmentTestDataGenerator.getSegment());
        int updatedCount = 1;
        assertThat(segmentDao.updateSegment(updateSegmentDTO.getSegmentId(), updateSegmentDTO), is(updatedCount));
        Optional<Segment> optional = getSegmentById(updateSegmentDTO.getSegmentId());
        assertThat(optional.isPresent(), is(true));
        assertThat(optional.get().getCity(), is(updateSegmentDTO.getCityPrefix()));
    }

    private Optional<Segment> getSegmentById(long segmentId) {
        return dslContext
                .selectFrom(SEGMENTS)
                .where(SEGMENTS.SEGMENT_ID.eq(segmentId))
                .fetchOptionalInto(Segment.class);
    }

    private List<Segment> getSegments() {
        return dslContext
                .selectFrom(SEGMENTS)
                .fetchInto(Segment.class);
    }
}
