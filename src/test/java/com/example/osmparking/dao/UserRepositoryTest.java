package com.example.osmparking.dao;

import com.example.osmparking.dao.impl.UserDaoImpl;
import com.example.osmparking.model.User;
import com.example.osmparking.model.dto.RegisterDto;
import org.jooq.DSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static com.example.osmparking.dao.TestDslContextSupplier.getConnection;
import static com.example.test.jooq.public_.tables.Users.USERS;
import static com.example.osmparking.UserTestData.registerUser;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("test")
public class UserRepositoryTest {

    private final RegisterDto registerDto = registerUser();
    private final DSLContext dslContext = getConnection();

    private final UserDao userDao = new UserDaoImpl(dslContext);

    @AfterEach
    void delete() {
        dslContext.deleteFrom(USERS).execute();
    }

    @Test
    public void registerUserTest() {
        userDao.register(registerDto, registerDto.password);
        Optional<User> user = userDao.findByLogin(registerDto.login);
        assertThat(user.isPresent(), is(true));
        assertThat(user.get().getLogin(), is(registerDto.login));
    }

    @Test
    public void findByLoginTest() {
        userDao.register(registerDto, registerDto.password);
        Optional<User> user = userDao.findByLogin(registerDto.login);
        assertThat(user.isPresent(), is(true));
        assertThat(user.get(), instanceOf(User.class));
        assertThat(user.get().getLogin(), is(registerDto.login));
        assertThat(user.get().getPassword(), is(registerDto.password));
    }
}
