package com.example.osmparking;

import com.example.osmparking.model.BoundingBox;
import com.example.osmparking.model.City;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.model.dto.CityGeometryDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static com.example.osmparking.GeometersDataContainer.*;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CityTestDataGenerator {

    public static String getRequestBodyFromFile(String path) {
        try {
            return Files.readString(Path.of(path));
        } catch (IOException e) {
            log.error("Illegal path to file {}", path, e);
            throw new IllegalArgumentException(e);
        }
    }

    public static CityDto cityDto() {
        return new CityDto("fds", "fds", 54.5645, -56.65684, 46.23598, 78.5564, "uojkl6j5kl");
    }

    public static BoundingBox boundingBox = new BoundingBox(52.3923449527382, 13.1874810501355, 52.6066036926653, 13.6642292106265);

    public static List<City> getListOfCities() {
        City city1 = new City(1, "fds", "fds", boundingBox, POLYGON_INTERSECTED);
        City city2 = new City(2, "ghfg", "tryty", boundingBox, POLYGON);
        return List.of(city1, city2);
    }

    public static List<CityDto> getListOfCitiesDto() {
        CityDto city1 = new CityDto("fds", "fds", 54.5645,
                -56.65684, 46.23598, 78.5564, POLYGON_INTERSECTED);
        CityDto city2 = new CityDto("ghfg", "tryty", 54.5645,
                -56.65684, 46.23598, 78.5564, POLYGON);
        return List.of(city1, city2);
    }

    public static List<CityGeometryDto> getCityGeometryDto() {
        CityGeometryDto cityPolygonDto1 = new CityGeometryDto("fds", getGeometry(POLYGON_INTERSECTED).get());
        CityGeometryDto cityPolygonDto2 = new CityGeometryDto("city", getGeometry(POLYGON).get());
        return List.of(cityPolygonDto1, cityPolygonDto2);
    }

    public static Optional<Geometry> getGeometry(String stringGeometry) {
        try {
            return Optional.of(new WKTReader().read(stringGeometry));
        } catch (Exception e) {
            log.error("Can't read geometry {}", stringGeometry, e);
            throw new IllegalArgumentException(e);
        }
    }

    public static List<CityDto> getListOfCitiesDtoFromDB() {
        CityDto city1 = new CityDto("fds", "fds", 54.5645,
                -56.65684, 46.23598, 78.5564, INTERSECTED_REGION);
        CityDto city2 = new CityDto("city", "city", 54.5645,
                -56.65684, 46.23598, 78.5564, REGION);
        return List.of(city1, city2);
    }
}
