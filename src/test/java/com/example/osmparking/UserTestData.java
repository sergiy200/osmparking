package com.example.osmparking;

import com.example.osmparking.model.User;
import com.example.osmparking.model.dto.LoginDto;
import com.example.osmparking.model.dto.RegisterDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserTestData {

    public static final String userRegRequest = "{\n" +
            "    \"login\":\"test\",\n" +
            "    \"password\":\"$2a$10$g2kFYM5PymR8vPSu868CY.cbZ3i5AWapwVPne8bMFoPiqEbWU6swa\",\n" +
            "    \"userName\":\"username\"\n" +
            "}";
    public static final String userLoginRequest = "{\n" +
            "    \"login\":\"test\",\n" +
            "    \"password\":\"test\"\n" +
            "}";
    public static Claims claims = new DefaultClaims(Map.of("sub", "test", "Authorization",
            "USER", "iat", "1636982347", "exp", "1636984147"));
    public static Map<String, Object> authClaim = Map.of("Authorization", "USER");

    public static LoginDto authorizationData() {
        return new LoginDto("test", "test");
    }

    public static RegisterDto registerUser() {
        return new RegisterDto("username", "test",
                "$2a$10$g2kFYM5PymR8vPSu868CY.cbZ3i5AWapwVPne8bMFoPiqEbWU6swa");
    }

    public static User userFromDao() {
        return new User(1, "username", "test",
                "$2a$10$g2kFYM5PymR8vPSu868CY.cbZ3i5AWapwVPne8bMFoPiqEbWU6swa", "USER");
    }
}
