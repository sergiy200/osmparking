package com.example.osmparking.service;

import com.example.osmparking.service.impl.CitySegmentServiceImpl;
import org.junit.jupiter.api.Test;

import static com.example.osmparking.CityTestDataGenerator.getCityGeometryDto;
import static com.example.osmparking.SegmentTestDataGenerator.getIntersectedCityPrefixDto;
import static com.example.osmparking.SegmentTestDataGenerator.getSegment;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CitySegmentServiceTest {

    private static final String FILE_NAME = "fileName";

    private final SegmentsService segmentsService = mock(SegmentsService.class);
    private final CityService cityService = mock(CityService.class);
    private final CitySegmentService citySegmentService = new CitySegmentServiceImpl(segmentsService, cityService);

    @Test
    public void setCityPrefixToSegmentsTest() {
        when(segmentsService.getSegmentsFromCSV(FILE_NAME)).thenReturn(getSegment());
        when(segmentsService.save(getSegment())).thenReturn(true);
        when(cityService.getCityGeometry()).thenReturn(getCityGeometryDto());
        assertThat(citySegmentService.save(FILE_NAME), is(true));
    }

    @Test
    public void setCityPrefixTest() {
        when(segmentsService.getCityPrefixWithIntersectedSegment()).thenReturn(getIntersectedCityPrefixDto());
        when(segmentsService.updateSegmentsCities(getIntersectedCityPrefixDto())).thenReturn(true);
        assertThat(segmentsService.updateSegmentsCities(segmentsService.getCityPrefixWithIntersectedSegment()), is(true));
    }
}
