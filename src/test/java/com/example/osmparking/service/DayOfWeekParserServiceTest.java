package com.example.osmparking.service;

import com.example.osmparking.model.enums.Side;
import com.example.osmparking.service.impl.DayOfWeekParserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.example.osmparking.SegmentTestDataGenerator.parkingTimeTags;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DayOfWeekParserServiceTest {

    DayOfWeekParserService dayOfWeekParserService = new DayOfWeekParserServiceImpl();

    @Test
    public void getAll() {
        assertThat(dayOfWeekParserService.getDayOfWeek("Tu"), is(2));
        assertThat(dayOfWeekParserService.getDayOfWeek("mo"), is(1));
        assertThat(dayOfWeekParserService.getDayOfWeek("We"), is(3));
    }

    @Test
    public void getFailDayOfWeek() {
        IllegalArgumentException exception = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> dayOfWeekParserService.getDayOfWeek("td")
        );
        String expectedMessage = "Can't get day td";
        assertThat(expectedMessage, is(exception.getMessage()));
        assertThat(IllegalArgumentException.class, is(exception.getClass()));
    }

    @Test
    public void getMapTagsTest() {
        assertThat(Side.LEFT.getParkingTime().apply(parkingTimeTags), is(Optional.ofNullable(parkingTimeTags.getParkingConditionLeftTime())));
        assertThat(Side.RIGHT.getParkingTime().apply(parkingTimeTags), is(Optional.ofNullable(parkingTimeTags.getParkingConditionRightTime())));
        assertThat(Side.BOTH.getParkingTime().apply(parkingTimeTags), is(Optional.ofNullable(parkingTimeTags.getParkingConditionBothTime())));
    }
}
