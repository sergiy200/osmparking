package com.example.osmparking.service;

import com.example.osmparking.dao.UserDao;
import com.example.osmparking.jwt.JwtUtil;
import com.example.osmparking.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static com.example.osmparking.UserTestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {

    private static final String JWT_TOKEN = UUID.randomUUID().toString();
    private final UserDao userDao = mock(UserDao.class);
    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    private final JwtUtil jwtUtil = mock(JwtUtil.class);
    private final UserService userService = new UserServiceImpl(userDao, passwordEncoder, jwtUtil);

    @Test
    public void registrationTest() {
        when(userDao.register(registerUser(), passwordEncoder.encode(registerUser().getPassword()))).thenReturn(1);
        assertThat(userService.register(registerUser()), is(true));
    }

    @Test
    public void registrationFailInsertTest() {
        when(userDao.register(registerUser(), registerUser().getPassword())).thenReturn(0);
        assertThat(userService.register(registerUser()), is(false));
    }

    @Test
    public void loginWithTokenTest() {
        when(userDao.findByLogin(authorizationData().getLogin())).thenReturn(Optional.ofNullable(userFromDao()));
        when(passwordEncoder.matches(authorizationData().getPassword(), registerUser().getPassword())).thenReturn(true);
        when(jwtUtil.generateToken(userFromDao())).thenReturn(JWT_TOKEN);
        assertThat(userService.login(authorizationData()), is(Optional.of(JWT_TOKEN)));
    }

    @Test
    public void generateEmptyTokenTest() {
        when(userDao.findByLogin(authorizationData().getLogin())).thenReturn(Optional.ofNullable(userFromDao()));
        when(passwordEncoder.matches(authorizationData().getPassword(), registerUser().getPassword())).thenReturn(true);
        when(jwtUtil.generateToken(userFromDao())).thenReturn(null);
        assertThat(userService.login(authorizationData()), is(Optional.empty()));
    }
}
