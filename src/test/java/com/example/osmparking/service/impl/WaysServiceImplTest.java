package com.example.osmparking.service.impl;

import com.example.osmparking.dao.ParkingTimeDao;
import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.dto.SegmentRestrictionTime;
import com.example.osmparking.service.GeometryService;
import com.example.osmparking.service.WaysService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.GeometersDataContainer.BUFFERED_POLYGON;
import static com.example.osmparking.GeometersDataContainer.BUFFERED_POLYGON_INTERSECTED;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class WaysServiceImplTest {
    private final GeometryService geometryService = new GeometryServiceImpl();
    private final ParkingTimeDao parkingTimeDao = mock(ParkingTimeDao.class);
    private final SegmentDao segmentDao = mock(SegmentDao.class);

    private final WaysService waysService = new WaysServiceImpl(geometryService, parkingTimeDao, segmentDao);

    @Test
    void getSegmentWithRestrictionTimes() {
        when(parkingTimeDao.getRestrictionTime(boundingBox)).thenReturn(new ArrayList<>(getRestrictionTimeDto()));
        when(segmentDao.getIntersectedSegmentGeometries(BUFFERED_POLYGON)).thenReturn(segmentWayGeometryDto());
        assertThat(waysService.getSegmentWithRestrictionTimes(boundingBox),
                is(List.of(new SegmentRestrictionTime(segmentWayGeometryDto().get(0).getSegmentId(), getRestrictionTimeDto()))));
    }

    @Test
    void getSegmentWithRestrictionTimesUnIntersected() {
        when(parkingTimeDao.getRestrictionTime(boundingBox)).thenReturn(new ArrayList<>(getRestrictionTimeDtoIntersected()));
        when(segmentDao.getIntersectedSegmentGeometries(BUFFERED_POLYGON_INTERSECTED)).thenReturn(segmentWayGeometryDtoIntersected());
//        assertThat(waysService.getSegmentWithRestrictionTimes(boundingBox),
//                is(List.of(new SegmentRestrictionTime(segmentWayGeometryDtoIntersected().get(0).getSegmentId(),
//                        Collections.singleton(new ArrayList<>(getRestrictionTimeDtoIntersected()).get(0))))));
    }

    @Test
    void getSegmentWithRestrictionTimesIntersected() {
        when(parkingTimeDao.getRestrictionTime(boundingBox)).thenReturn(new ArrayList<>(getRestrictionTimeDto()));
        when(segmentDao.getIntersectedSegmentGeometries(BUFFERED_POLYGON)).thenReturn(segmentWayGeometryDtoIntersected());
        assertThat(waysService.getSegmentWithRestrictionTimes(boundingBox),
                is(Collections.emptyList()));
    }
}