package com.example.osmparking.service.impl;

import com.example.osmparking.model.OSMWay;
import com.example.osmparking.model.TimeInterval;
import com.example.osmparking.model.Ways;
import com.example.osmparking.model.dto.NoParkingTags;
import com.example.osmparking.model.dto.ParkingTimeTags;
import com.example.osmparking.service.OSMApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OSMDataProviderImplTest {

    private static final String TIME = "Mo-Fr 07:00-17:00; Sa 07:00-13:00";
    private static final String RIGHT = "right";
    private static final String QUERY = "query";

    private final ObjectMapper objectMapper = mock(ObjectMapper.class);
    private final OSMApiService osmApiService = mock(OSMApiService.class);
    private final TimeParserServiceImpl timeParserService = mock(TimeParserServiceImpl.class);

    private final OSMDataProviderImpl osmDataProvider = new OSMDataProviderImpl(osmApiService,
            objectMapper, timeParserService);

    @Test
    void getNoParkingSegments() {
        when(osmApiService.getElements(boundingBox, QUERY, OSMWay.class)).thenReturn(getOSMWay());
        when(objectMapper.convertValue(tags, NoParkingTags.class)).thenReturn(new NoParkingTags("highway", "", "", "", "", "", ""));
        assertThat(osmDataProvider.getNoParkingSegments(boundingBox, QUERY), containsInAnyOrder(getNoParkingSegment().get(INTEGER_ZERO), getNoParkingSegment().get(INTEGER_ONE)));
    }

    @Test
    void getRestrictionTimes() {
        List<TimeInterval> timeInterval = getTimeInterval();
        when(osmApiService.getElements(boundingBox, QUERY, OSMWay.class)).thenReturn(getOSMWay());
        when(objectMapper.convertValue(tags, ParkingTimeTags.class)).thenReturn(parkingTimeTags);
        when(timeParserService.getTimeInterval(TIME)).thenReturn(timeInterval);
        when(timeParserService.createRestrictionTime(INTEGER_ONE, timeInterval, RIGHT)).thenReturn(getParkingTimeWays());
        assertThat(osmDataProvider.getRestrictionTimes(boundingBox, QUERY), containsInAnyOrder(getParkingTimeWays().get(INTEGER_ZERO), getParkingTimeWays().get(INTEGER_ONE)));
    }

    @Test
    void getWays() {
        List<Ways> waysList = getWaysList();
        when(osmApiService.getElements(boundingBox, QUERY, OSMWay.class)).thenReturn(getOSMWay());
        assertThat(osmDataProvider.getWays(boundingBox, QUERY), containsInAnyOrder(waysList.get(INTEGER_ZERO), waysList.get(INTEGER_ONE)));
    }
}