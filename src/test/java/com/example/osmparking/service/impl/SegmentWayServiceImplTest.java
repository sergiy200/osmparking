package com.example.osmparking.service.impl;

import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.dto.SegmentRestrictionTime;
import com.example.osmparking.service.SegmentWayService;
import com.example.osmparking.service.WaysService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SegmentWayServiceImplTest {

    private final WaysService waysService = mock(WaysService.class);
    private final SegmentDao segmentDao = mock(SegmentDao.class);
    private final SegmentWayService segmentWayService = new SegmentWayServiceImpl(waysService, segmentDao);

    @Test
    void getGeoJsonRestrictionSegment() {
        List<SegmentRestrictionTime> value = List.of(new SegmentRestrictionTime(segmentWayGeometryDtoIntersected().get(0).getSegmentId(),
                Collections.singleton(new ArrayList<>(getRestrictionTimeDtoIntersected()).get(0))));
        when(waysService.getSegmentWithRestrictionTimes(boundingBox)).thenReturn(
                value);
        value.forEach(v -> when(segmentDao.getSegments(v.getSegmentId())).thenReturn(getWaysGeoJson()));
//        assertThat(segmentWayService.getGeoJsonRestrictionSegment(boundingBox), containsInAnyOrder(segmentGeoJsonRestrictionTime().get(0)));
    }
}