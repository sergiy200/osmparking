package com.example.osmparking.service;


import com.example.osmparking.service.impl.DayOfWeekParserServiceImpl;
import com.example.osmparking.service.impl.TimeServiceImpl;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TimeServiceTest {

    private final DayOfWeekParserService dayOfWeekParserService = new DayOfWeekParserServiceImpl();
    private final TimeService timeService = new TimeServiceImpl(dayOfWeekParserService);

    @Test
    public void getDaysOfWeekTest() {
        List<String[]> st = new ArrayList<>();
        String timeInterval = "18:00-21:00";
        st.add(new String[]{getDay(timeInterval, INTEGER_ZERO), getDay(timeInterval, INTEGER_ONE)});
        assertThat(Arrays.equals(st.get(INTEGER_ZERO), timeService.getTime(timeInterval).get(INTEGER_ZERO)), is(true));
    }

    @Test
    public void getDaysOfWeek() {
        String monday = "Mo";
        String daysRangeSaTu = "Sa-Tu";
        String daysRangeMoFr = "Mo-Fr";
        assertThat(Arrays.equals(new Integer[]{1}, timeService.getDaysOfWeek(monday)), is(true));
        assertThat(Arrays.equals(new Integer[]{1, 2, 3, 4, 5}, timeService.getDaysOfWeek(daysRangeMoFr)), is(true));
        assertThat(Arrays.equals(new Integer[]{6, 7, 1, 2}, timeService.getDaysOfWeek(daysRangeSaTu)), is(true));
    }

    @Test
    public void getLocalTimeTest() {
        assertThat(timeService.getLocalTime("24:00"), is(LocalTime.of(23, 59, 59)));
        assertThat(timeService.getLocalTime("23:00"), is(LocalTime.of(23, 0)));
        assertThat(timeService.getLocalTime("15:30"), is(LocalTime.of(15, 30)));
        assertThat(timeService.getLocalTime("00:00"), is(LocalTime.of(0, 0)));
    }

    private String getDay(String daysRangeSaTu, int x) {
        return daysRangeSaTu.split("-")[x];
    }
}
