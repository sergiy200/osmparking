package com.example.osmparking.service;

import com.example.osmparking.model.TimeInterval;
import com.example.osmparking.service.impl.TimeParserServiceImpl;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.List;

import static com.example.osmparking.SegmentTestDataGenerator.getParkingTimeWays;
import static com.example.osmparking.SegmentTestDataGenerator.getTimeInterval;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TimeParserServiceTest {

    private static final String TIME_INTERVAL = "Mo 17:00-18:00";
    private static final String RIGHT = "right";
    private static final String FROM_TIME = "17:00";
    private static final String TO_TIME = "18:00";
    private static final String ALL_WEEK = "Mo-Su";

    private final TimeService timeService = mock(TimeService.class);
    private final TimeParserService timeParserService = new TimeParserServiceImpl(timeService);

    @Test
    public void getTimeIntervalTimeMarket1Test() {
        List<TimeInterval> timeInterval = getTimeInterval();
        when(timeService.getTime(getString(TIME_INTERVAL, INTEGER_ONE))).thenReturn(timeInterval.get(INTEGER_ZERO).getTimeInterval());
        when(timeService.getDaysOfWeek(getString(TIME_INTERVAL, INTEGER_ZERO))).thenReturn(timeInterval.get(INTEGER_ZERO).getDaysOfWeek());
        assertThat(timeParserService.getTimeInterval(TIME_INTERVAL), is(timeInterval));
    }

    @Test
    public void getTimeIntervalTimeMarket2Test() {
        List<TimeInterval> timeInterval = getTimeInterval();
        String timeRange = "15:00-18-00";
        when(timeService.getTime(getString(timeRange, INTEGER_ZERO)))
                .thenReturn(timeInterval.get(INTEGER_ZERO).getTimeInterval());
        when(timeService.getDaysOfWeek(ALL_WEEK)).thenReturn(timeInterval.get(INTEGER_ZERO).getDaysOfWeek());
        assertThat(timeParserService.getTimeInterval(timeRange), is(timeInterval));
    }

    @Test
    public void getTimeIntervalTimeMarket3Test() {
        String twoIntervals = "Mo 17:00-18:00; Su-Mo 05:00-11:00";
        String partOfInterval = "Su-Mo 05:00-11:00";
        List<TimeInterval> timeInterval = getTimeInterval();
        when(timeService.getTime(getString(TIME_INTERVAL, INTEGER_ONE)))
                .thenReturn(timeInterval.get(INTEGER_ZERO).getTimeInterval());
        when(timeService.getTime(getString(partOfInterval, INTEGER_ONE)))
                .thenReturn(timeInterval.get(INTEGER_ZERO).getTimeInterval());
        when(timeService.getDaysOfWeek(getString(TIME_INTERVAL, INTEGER_ZERO)))
                .thenReturn(timeInterval.get(INTEGER_ZERO).getDaysOfWeek());
        when(timeService.getDaysOfWeek(getString(partOfInterval, INTEGER_ZERO)))
                .thenReturn(timeInterval.get(INTEGER_ZERO).getDaysOfWeek());
        assertThat(timeParserService.getTimeInterval(twoIntervals),
                is(List.of(timeInterval.get(INTEGER_ZERO), timeInterval.get(INTEGER_ZERO))));
    }

    @Test
    public void getRestrictionByParkingTimeTagsTest() {
        int wayId = 1;
        String timeTo2 = "10:00";
        String timeFrom2 = "05:00";
        List<TimeInterval> timeInterval = getTimeInterval();
        when(timeService.getLocalTime(FROM_TIME)).thenReturn(LocalTime.parse(FROM_TIME));
        when(timeService.getLocalTime(TO_TIME)).thenReturn(LocalTime.parse(TO_TIME));
        when(timeService.getLocalTime(timeFrom2)).thenReturn(LocalTime.parse(timeFrom2));
        when(timeService.getLocalTime(timeTo2)).thenReturn(LocalTime.parse(timeTo2));
        assertThat(timeParserService.createRestrictionTime(wayId, timeInterval, RIGHT),
                containsInAnyOrder(getParkingTimeWays().get(INTEGER_ZERO), getParkingTimeWays().get(INTEGER_ONE)));
    }

    private static String getString(String timeInterval, int x) {
        return timeInterval.split("\\s")[x];
    }
}
