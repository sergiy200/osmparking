package com.example.osmparking.service;

import com.example.osmparking.dao.ParkingTimeDao;
import com.example.osmparking.dao.SegmentDao;
import com.example.osmparking.model.Segment;
import com.example.osmparking.service.impl.SegmentsServiceImpl;
import org.junit.jupiter.api.Test;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.CityTestDataGenerator.getCityGeometryDto;
import static com.example.osmparking.SegmentTestDataGenerator.*;
import static com.example.osmparking.model.utils.QueryUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class SegmentServiceTest {

    private static final String BUCKET_SEGMENTS = "bucketSegments";

    private static final String FILE_NAME = "fileName";

    private final S3ReaderService s3ReaderService = mock(S3ReaderService.class);
    private final SegmentDao segmentDao = mock(SegmentDao.class);
    private final ParkingTimeDao parkingTimeDao = mock(ParkingTimeDao.class);
    private final OSMDataProvider osmDataProvider = mock(OSMDataProvider.class);
    private final SegmentsService segmentsService = new SegmentsServiceImpl(s3ReaderService,
            segmentDao, BUCKET_SEGMENTS, parkingTimeDao, osmDataProvider);

    @Test
    public void getSegmentsFromCSVTest() {
        int lineToSkip = 0;
        when(s3ReaderService.getDataFromBucket(BUCKET_SEGMENTS, FILE_NAME, Segment.class, lineToSkip)).thenAnswer(x -> getSegment());
        assertThat(segmentsService.getSegmentsFromCSV(FILE_NAME), is(getSegment()));
    }

    @Test
    public void saveSegmentsTest() {
        when(segmentDao.save(getSegment())).thenReturn(getSegment().size());
        assertThat(segmentsService.save(getSegment()), is(true));
    }

    @Test
    public void getCityNameWithIntersectedSegmentTest() {
        when(segmentDao.getCityPrefixWithIntersectedSegments()).thenReturn(getIntersectedCityPrefixDto());
        assertThat(segmentsService.getCityPrefixWithIntersectedSegment(), is(getIntersectedCityPrefixDto()));
    }

    @Test
    public void setCityPrefixToSegmentsTest() {
        segmentsService.setCityPrefixToSegments(segmentWayGeometryDto(), getCityGeometryDto());
        verify(segmentDao).updateSegmentCities(getIntersectedCityPrefixDto());
    }

    @Test
    public void updateSegment() {
        long segmentId = updateSegmentDTO.getSegmentId();
        when(segmentDao.updateSegment(segmentId, updateSegmentDTO)).thenReturn(1);
        assertThat(segmentsService.updateSegment(updateSegmentDTO), is(true));
    }

    @Test
    public void saveNoParkingSegmentsTest() {
        int countSavedDataAndFromOSM = 2;
        when(osmDataProvider.getNoParkingSegments(boundingBox, GET_NO_PARKING)).thenReturn(getNoParkingSegment());
        when(segmentDao.saveParkingWays(getNoParkingSegment())).thenReturn(countSavedDataAndFromOSM);
        assertThat(segmentsService.saveNoParkingSegments(boundingBox), is(true));
    }

    @Test
    public void getNoParkingSegmentTest() {
        int firstElement = 0;
        int secondElement = 1;
        when(segmentDao.getNoParking()).thenReturn(getNoParkingSegment());
        assertThat(segmentDao.getNoParking(),
                containsInAnyOrder(getNoParkingSegment().get(firstElement), getNoParkingSegment().get(secondElement)));
    }

    @Test
    public void saveRestrictionTimeTest() {
        int countSavedDataAndFromOSM = 2;
        when(parkingTimeDao.saveRestrictionTime(getParkingTimeWays())).thenReturn(countSavedDataAndFromOSM);
        when(osmDataProvider.getRestrictionTimes(boundingBox, GET_NO_PARKING_TIME)).thenReturn(getParkingTimeWays());
        assertThat(segmentsService.saveRestrictionTime(boundingBox), is(countSavedDataAndFromOSM));
    }

    @Test
    public void getWays() {
        when(segmentDao.getWays(boundingBox)).thenReturn(getWaysGeoJson());
        assertThat(segmentsService.getWays(boundingBox), is(geoJsonFeatureCollection()));
    }

    @Test
    public void getNoParkingMapBox() {
        when(segmentDao.getNoParkingMapBox(boundingBox)).thenReturn(getWaysGeoJson());
        assertThat(segmentsService.getNoParkingMapBox(boundingBox), is(geoJsonFeatureCollection()));
    }
    @Test
    public void saveAllWays() {
        when(osmDataProvider.getWays(boundingBox, GET_ALL_WAYS)).thenReturn(getWaysList());
        int countSavedElements = 2;
        when(segmentDao.saveWays(getWaysList())).thenReturn(countSavedElements);
        assertThat(segmentsService.saveAllWays(boundingBox), is(countSavedElements));
        verify(segmentDao).deleteWays(boundingBox);
    }
}
