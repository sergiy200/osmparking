package com.example.osmparking.service;

import com.example.osmparking.dao.CityDao;
import com.example.osmparking.model.dto.CityDto;
import com.example.osmparking.service.impl.CityServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.example.osmparking.CityTestDataGenerator.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CityServiceImplTest {

    private static final String BUCKET_CITIES = "bucketCities";

    private static final String PREFIX = "prefix";

    private static final String FILE_NAME = "fileName";

    private final CityDao cityDao = mock(CityDao.class);
    private final S3ReaderService s3ReaderService = mock(S3ReaderService.class);
    private final CityService cityService = new CityServiceImpl(cityDao, s3ReaderService, BUCKET_CITIES);

    @Test
    public void getAllTest() {
        when(cityDao.getAll()).thenReturn(getListOfCities());
        assertThat(cityService.getAll(), is(getListOfCities()));
    }

    @Test
    public void readCSVReturnListSaveCitiesTest() {
        int lineToSkip = 1;
        when(s3ReaderService.getDataFromBucket(BUCKET_CITIES, FILE_NAME, CityDto.class, lineToSkip)).thenAnswer(x -> getListOfCitiesDto());
        when(cityDao.saveCities(getListOfCitiesDto())).thenReturn(getListOfCitiesDto().size());
        assertThat(cityService.saveCitiesFromCSV(FILE_NAME), is(true));
    }

    @Test
    public void saveCitiesTest() {
        int lineToSkip = 1;
        when(s3ReaderService.getDataFromBucket(BUCKET_CITIES, FILE_NAME, CityDto.class, lineToSkip)).thenAnswer(x -> getListOfCitiesDto());
        when(cityDao.saveCities(getListOfCitiesDto())).thenReturn(0);
        assertThat(cityService.saveCitiesFromCSV(FILE_NAME), is(false));
    }

    @Test
    public void getCitiesTest() {
        when(cityDao.getCities(PREFIX)).thenReturn(getListOfCities());
        assertThat(cityService.getCities(PREFIX), is(getListOfCities()));
    }

    @Test
    public void getEmptyCitiesListTest() {
        when(cityDao.getCities(PREFIX)).thenReturn(List.of());
        assertThat(cityService.getCities(PREFIX), is(List.of()));
    }

    @Test
    public void deleteTest() {
        when(cityDao.delete(PREFIX)).thenReturn(1);
        assertThat(cityService.delete(PREFIX), is(true));
    }

    @Test
    public void addCityTest() {
        when(cityDao.saveCities(List.of(cityDto()))).thenReturn(1);
        assertThat(cityService.addCity(cityDto()), is(true));
    }
}
