package com.example.osmparking.service;

import com.example.osmparking.jwt.JwtUtil;
import io.jsonwebtoken.*;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.Date;
import java.util.UUID;

import static com.example.osmparking.UserTestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

public class JwtUtilTest {

    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String JWT_TOKEN = UUID.randomUUID().toString();
    private static String secretKey = "secretKey";
    private static long sessionTime = 2000;

    private final TimeService timeService = mock(TimeService.class);
    private final JwtUtil jwtUtil = new JwtUtil(timeService, secretKey, sessionTime);
    private ArgumentCaptor strCaptor = ArgumentCaptor.forClass(JwtUtil.class);

    @Test
    public void getUsernameTest() {
        try (MockedStatic<Jwts> jwtsMockedStatic = Mockito.mockStatic(Jwts.class)) {
            JwtParser parser = mock(JwtParser.class);
            jwtsMockedStatic.when(Jwts::parser).thenReturn(parser);
            Jws<Claims> jws = mock(Jws.class);
            when(parser.setSigningKey(secretKey)).thenReturn(parser);
            when(parser.parseClaimsJws(JWT_TOKEN)).thenReturn(jws);
            when(jws.getBody()).thenReturn(claims);
            String userName = jwtUtil.getUsername(JWT_TOKEN);
            assertThat(userName, is(userFromDao().getLogin()));
        }
    }

    @Test
    public void getUsernameCaptureTest() {
        try (MockedStatic<Jwts> jwtsMockedStatic = Mockito.mockStatic(Jwts.class)) {
            JwtParser parser = mock(JwtParser.class);
            jwtsMockedStatic.when(Jwts::parser).thenReturn(parser);
            Jws<Claims> jws = mock(Jws.class);
            when(parser.setSigningKey(secretKey)).thenReturn(parser);
            when(parser.parseClaimsJws(JWT_TOKEN)).thenReturn(jws);
            when(jws.getBody()).thenReturn(claims);
            jwtUtil.getUsername(JWT_TOKEN);
            verify(parser).parseClaimsJws((String) strCaptor.capture());
            assertThat(strCaptor.getValue(), is(JWT_TOKEN));
        }
    }

    @Test
    public void getAuthoritiesTest() {
        try (MockedStatic<Jwts> jwtsMockedStatic = Mockito.mockStatic(Jwts.class)) {
            JwtParser parser = mock(JwtParser.class);
            jwtsMockedStatic.when(Jwts::parser).thenReturn(parser);
            Jws<Claims> jws = mock(Jws.class);
            when(parser.setSigningKey(secretKey)).thenReturn(parser);
            when(parser.parseClaimsJws(JWT_TOKEN)).thenReturn(jws);
            when(jws.getBody()).thenReturn(claims);
            String auth = jwtUtil.getAuthorities(JWT_TOKEN);
            assertThat(auth, is(userFromDao().getRole()));
        }
    }

    @Test
    public void getTokenWithoutPrefix() {
        when(timeService.getCurrentTimeMillis()).thenReturn(new Date(sessionTime));
        try (MockedStatic<Jwts> jwtsMockedStatic = mockStatic(Jwts.class)) {
            JwtBuilder builder = mock(JwtBuilder.class);
            jwtsMockedStatic.when(Jwts::builder).thenReturn(builder);
            when(builder.setSubject(userFromDao().getLogin())).thenReturn(builder);
            when(builder.addClaims(authClaim)).thenReturn(builder);
            when(builder.setIssuedAt(new Date(sessionTime))).thenReturn(builder);
            when(builder.setExpiration(new Date(sessionTime + sessionTime))).thenReturn(builder);
            when(builder.signWith(SignatureAlgorithm.HS256, secretKey)).thenReturn(builder);
            when(builder.compact()).thenReturn(JWT_TOKEN);
            assertThat(jwtUtil.generateToken(userFromDao()), is(TOKEN_PREFIX + JWT_TOKEN));
        }
    }
}
