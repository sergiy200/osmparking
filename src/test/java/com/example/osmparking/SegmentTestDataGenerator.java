package com.example.osmparking;

import com.example.osmparking.model.*;
import com.example.osmparking.model.dto.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;

import java.time.LocalTime;
import java.util.*;

import static com.example.osmparking.CityTestDataGenerator.boundingBox;
import static com.example.osmparking.CityTestDataGenerator.getGeometry;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SegmentTestDataGenerator {

    public static UpdateSegmentDTO updateSegmentDTO = new UpdateSegmentDTO(465,
            5,
            45,
            "[ {\"lon\": 12.606300927176, \"lat\": 55.6616778972662},{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}]",
            "0102000020E610000002000000F4A95857F513294076A952F2F2D54B40FC23783AFB132940E364B5C2F7D54B40",
            "fds",
            123,
            "dfsdf",
            "sdfsdf",
            "{\"transportMask\": {\"flags\": 6}}",
            "0101000000F4B5BCBA2A3429402FA5DBE2D3D64B40",
            "sdfsdfsd");

    public static List<Segment> getSegment() {
        Segment segment1 = new Segment(4564, 4564, 87979,
                " {\"lon\": 12.606300927176, \"lat\": 55.6616778972662}",
                "  {\"lon\": 12.606300927176, \"lat\": 55.6616778972662}",
                "[ {\"lon\": 12.606300927176, \"lat\": 55.6616778972662},{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}]",
                "0102000020E61000000300000033C47194F70F3240B2754CA08BAB4D403EA26AD2FF0F3240B512B7D38CAB4D40AE6469C65510324045FFDFC897AB4D40",
                "sdfsdfsdfs",
                5, "sdfsdfsd",
                "sdfsdfsd",
                "{\"lon\": 12.606300927176, \"lat\": 55.6616778972662}",
                " {\"lon\": 12.606300927176, \"lat\": 55.6616778972662}",
                "0101000000110F5A94051E29408E720B86AAD74B40", "sdfds");

        Segment segment2 = new Segment(465, 892, 364,
                "{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}",
                " {\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}",
                "[{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}]",
                "0102000020E61000000300000033C47194F70F3240B2754CA08BAB4D403EA26AD2FF0F3240B512B7D38CAB4D40AE6469C65510324045FFDFC897AB4D40",
                "roadType",
                5, "highway",
                "StreetName",
                "{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}",
                "{\"lon\": 12.6051241341538,  \"lat\": 55.6612218463407}",
                "0101000000110F5A94051E29408E720B86AAD74B40", "sdfds");
        return List.of(segment1, segment2);
    }

    public static List<SegmentWayGeometryDto> segmentWayGeometryDto() {
        SegmentWayGeometryDto segmentWayGeometryDto = new SegmentWayGeometryDto(4564,
                getGeometry("LINESTRING(18.0623715188742 59.34019855245,18.0624972830139 59.3402351993476,18.0638088233288 59.3405696004616)").get());
        return List.of(segmentWayGeometryDto);
    }

    public static List<SegmentCityPrefixDto> getIntersectedCityPrefixDto() {
        SegmentCityPrefixDto segmentCityPrefixDto = new SegmentCityPrefixDto(4564, "fds");
        return List.of(segmentCityPrefixDto);
    }

    public static List<OSMElement> getOSMElement() {
        OSMElement element = new OSMElement(1, 1, "", "", 1123, 45654, new Date(),
                45, 45645, List.of(), new LinkedHashMap(), List.of(new LinkedHashMap()));
        return List.of(element, element);
    }

    public static LinkedHashMap<String, String> tags = new LinkedHashMap<>(Map.of("parking:condition:right:time_interval", "Mo-Fr 06:00-17:00",
            "parking:condition:right", "Mo-Fr 06:00-17:00",
            "highway", "highway",
            "sdcvxcv", "higy",
            "parking:condition:both:time_interval", ""));

    public static List<OSMWay> getOSMWay() {

        OSMWay element = new OSMWay(1, "way", boundingBox, List.of(), List.of(new Point(52.494800567627, 13.5204515457153),
                new Point(52.494758605957, 13.5212926864624)),
                tags);
        OSMWay element1 = new OSMWay(5, "way", boundingBox, List.of(), List.of(new Point(52.492057800293, 13.5061187744141),
                new Point(52.4914932250977, 13.5058431625366)),
                tags);
        return List.of(element, element1);
    }

    public static List<NoParkingSegment> getNoParkingSegment() {
        NoParkingSegment noParkingSegment = new NoParkingSegment(1, "LINESTRING (13.5204515457153 52.494800567627, 13.5212926864624 52.494758605957)",
                new NoParkingTags("highway", "", "", "", "", "", ""));
        NoParkingSegment noParkingSegment1 = new NoParkingSegment(5, "LINESTRING (13.5061187744141 52.492057800293, 13.5058431625366 52.4914932250977)",
                new NoParkingTags("highway", "", "", "", "", "", ""));
        return List.of(noParkingSegment, noParkingSegment1);
    }

    public static MultiValueMap<String, String> bboxRequestParams = new MultiValueMapAdapter<>(
            Map.of("leftLatitude", List.of(String.valueOf(boundingBox.getLeftLatitude())),
                    "leftLongitude", List.of(String.valueOf(boundingBox.getLeftLongitude())),
                    "rightLatitude", List.of(String.valueOf(boundingBox.getRightLatitude())),
                    "rightLongitude", List.of(String.valueOf(boundingBox.getRightLongitude()))));

    public static List<RestrictionTime> getParkingTimeWays() {
        RestrictionTime restrictionTime = new RestrictionTime(1, new Integer[]{1, 2, 3}, getOf(17), getOf(18), "right");
        RestrictionTime restrictionTime1 = new RestrictionTime(1, new Integer[]{1, 2, 3}, getOf(5), getOf(10), "right");
        return List.of(restrictionTime, restrictionTime1);
    }

    private static LocalTime getOf(int hour) {
        return LocalTime.of(hour, 0);
    }

    public static List<TimeInterval> getTimeInterval() {
        TimeInterval timeInterval = new TimeInterval(new Integer[]{1, 2, 3}, List.of((new String[]{"17:00", "18:00"}), (new String[]{"05:00", "10:00"})));
        return List.of(timeInterval);
    }

    public static ParkingTimeTags parkingTimeTags = new ParkingTimeTags("Mo-Fr 07:00-17:00; Sa 07:00-13:00", null, null);

    public static List<GeoJson> getWaysGeoJson() {
        return List.of(new GeoJson("LineString", List.of(List.of(12.1, 15.5))),
                new GeoJson("LineString", List.of(List.of(35.5, 26.7))));
    }

    public static GeoJsonFeatureCollection geoJsonFeatureCollection() {
        return new GeoJsonFeatureCollection(List.of(new GeoJsonFeature(getWaysGeoJson().get(0)), new GeoJsonFeature(getWaysGeoJson().get(1))));
    }

    public static List<Ways> getWaysList() {
        Ways ways = new Ways(1, "LINESTRING (13.5204515457153 52.494800567627, 13.5212926864624 52.494758605957)", "highway");
        Ways ways1 = new Ways(5, "LINESTRING (13.5061187744141 52.492057800293, 13.5058431625366 52.4914932250977)", "highway");
        return List.of(ways, ways1);
    }

    public static Set<RestrictionTimeDto> getRestrictionTimeDto() {
        RestrictionTimeDto restrictionTimeDto = new RestrictionTimeDto(12, "LINESTRING(18.0623715188742 59.34019855245,18.0624972830139 59.3402351993476,18.0638088233288 59.3405696004616)", List.of(1, 2, 3, 4), "01:20", "15:30", "left");
        RestrictionTimeDto restrictionTimeDto1 = new RestrictionTimeDto(10, "LINESTRING(18.0623715188742 59.34019855245,18.0624972830139 59.3402351993476,18.0638088233288 59.3405696004616)", List.of(1, 2, 3, 4), "01:20", "15:30", "right");
        return Set.of(restrictionTimeDto, restrictionTimeDto1);
    }

    public static Set<RestrictionTimeDto> getRestrictionTimeDtoIntersected() {
        RestrictionTimeDto restrictionTimeDto = new RestrictionTimeDto(12, "LINESTRING(13.4336674 52.5125325,13.4338238 52.5127189,13.4338988 52.5128091)", List.of(1, 2, 3, 4), "01:20", "15:30", "left");
        RestrictionTimeDto restrictionTimeDto1 = new RestrictionTimeDto(10, "LINESTRING(18.0623715188742 59.34019855245,18.0624972830139 59.3402351993476,18.0638088233288 59.3405696004616)", List.of(1, 2, 3, 4), "01:20", "15:30", "right");
        return Set.of(restrictionTimeDto, restrictionTimeDto1);
    }

    public static List<SegmentWayGeometryDto> segmentWayGeometryDtoIntersected() {
        SegmentWayGeometryDto segmentWayGeometryDto = new SegmentWayGeometryDto(123,
                getGeometry("LINESTRING(13.4336582870029 52.5125228130369,13.433819983754 52.5127086919753,13.4339008321296 52.5128070981538)").get());
        return List.of(segmentWayGeometryDto);
    }

    public static List<RestrictionTimeDto> getRestrictionTimeDtoList() {
        RestrictionTimeDto restrictionTimeDto = new RestrictionTimeDto(1, "LINESTRING(13.5204515457153 52.494800567627,13.5212926864624 52.494758605957)", List.of(1, 2, 3), "17:00", "18:00", "right");
        RestrictionTimeDto restrictionTimeDto1 = new RestrictionTimeDto(1, "LINESTRING(13.5204515457153 52.494800567627,13.5212926864624 52.494758605957)", List.of(1, 2, 3), "05:00", "10:00", "right");
        return List.of(restrictionTimeDto, restrictionTimeDto1);
    }

    public static List<SegmentGeoJsonRestrictionTime> segmentGeoJsonRestrictionTime() {
        SegmentGeoJsonRestrictionTime segmentGeoJsonRestrictionTime = new SegmentGeoJsonRestrictionTime(123, new GeoJsonFeature(getWaysGeoJson().get(0)), List.of(
                new WayRestrictionTime(
                        new WayDto(10, "LINESTRING(18.0623715188742 59.34019855245,18.0624972830139 59.3402351993476,18.0638088233288 59.3405696004616)"),
                        List.of(new RestrictionTimeIntervalDto(List.of(1, 2, 3, 4), "01:20", "15:30", "right")))));
        SegmentGeoJsonRestrictionTime segmentGeoJsonRestrictionTime1 = new SegmentGeoJsonRestrictionTime(1, new GeoJsonFeature(getWaysGeoJson().get(0)), List.of());
        return List.of(segmentGeoJsonRestrictionTime, segmentGeoJsonRestrictionTime1);
    }
}
